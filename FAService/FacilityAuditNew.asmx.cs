﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;
using System.Xml;
using System.Text;
using Kowni.Common.BusinessLogic;
using System.Web.Script.Serialization;
using System.IO;
using System.Web.Script.Services;
using FAService.BaseBoClass;

using System.Net.Mail;
using Newtonsoft.Json;

using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Drawing;
using OfficeOpenXml.Drawing.Chart;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;



namespace FAService
{
    /// <summary>
    /// Summary description for FacilityAudit
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FacilityAuditNew : System.Web.Services.WebService
    {

        KCB.BLSingleSignOn objSingleSignOn = new KCB.BLSingleSignOn();
        KCB.BLAnnouncement objAnnouncement = new KCB.BLAnnouncement();
        BaseBO objbasebo = new BaseBO();

        KB.BLUser.BLUserCompany objuserCompany = new KB.BLUser.BLUserCompany();
        KB.BLUser.BLUserLocation objuserLocation = new KB.BLUser.BLUserLocation();

        [WebMethod]
        public string HelloWorld()
        {
            //DataSet dsRawdatas = GetRawDatasDateWiseReport(DateTime.Now, 2571, 5, 2);
            //string filename = Guid.NewGuid().ToString();
            //if (dsRawdatas.Tables[3].Rows.Count > 0)
            //{
            //    DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
            //    filename = drfilename["locsitename"].ToString();
            //}
            //string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";       

            //MakeExcel(dsRawdatas, "88.00", excelfilepath);

            //string CcMail = ",,,hemant.sahoo@dtss.in,jpatil@nse.co.in,ghanshyam.gaikwad@dtss.in,sanjeev.kumar@dtss.in,pinakin.solanki@dtss.in,sukhpreet.sidhu@dtss.in,,,sandeep.jagtap@dtss.in,suresh.gosavi@dtss.in,ghanshyam.gaikwad@dtss.in,rekha.prosper@dtss.in,madhavan.govindaraj@dtss.in,Pragati.bakshi@dtss.in,kunal.jasani@dtss.in,madhavan.govindaraj@dtss.in,stephenson.f@dtss.in,selvam.chokkappa@dtss.in,NHOperations@dtss.in,suresh.gosavi@dtss.in";
            //string[] emailaddress = CcMail.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
            //foreach (var address in emailaddress)
            //{
            //    if (address != string.Empty)
            //    {

            //    }
            //}


            //   SendMail(Convert.ToDateTime("22-11-2016"), 1207, 4034, 11, 2
            //, "Glasses of entire premises were not effectively cleaned as the rubber of glass cleaning applicator is not proper/new glass applicator to be provided at site. Spottings seen on the carpet were not effectively cleaned through weekend deep cleaning schedule. Workmen Register to be updated. Shortages of 2 staff out of 49 manpower to be filled ASAP."
            //, "N/A"
            //, "N/A"
            //, "ESIC cards not issued to new joined staff."
            //, "N/A"
            //, "N/A"
            //, "", 10, "89.22", "Accenture PDC4 - L246", "Audit reports and closers need to be shared. ");





            //SendMail(Convert.ToDateTime("28-11-2016"), 1207, 4496, 11, 2
            //, "Lack of ownership and poor monitoring observed within the supervisor's on maintaining the required standard at site. Grooming standard is not upto the mark and not being checked by the supervisors before deployment. Staff found updating deployment register is a serious concern. Effective cleaning is required for the entire glasses of premises. Spottings seen on the carpet were not effectively cleaned through weekend deep cleaning schedule. Effective cleaning is required in workstation area, reception area and wall spottings cleaning. Shortages of 2 staff out of 25 manpower to be filled ASPS as client has given deadline till 30th November 2016."
            //, "Supervisors required more training on their roles and responsibilities."
            //, "N/A"
            //, "N/A"
            //, "N/A"
            //, "N/A"
            //, "", 10, "85.81", "IBM 2nd and 4th Floor - L267", "More training required for supervisors to improve cleaning standard at site.");

            return "Hello World";
        }

        [WebMethod]
        public string GetLoginDetails(string username, string password)
        {
            //string memString = File.ReadAllText(Server.MapPath("AppThemes/images/datareport.html"));
            //// convert string to stream
            //byte[] buffer = Encoding.ASCII.GetBytes(memString);
            //MemoryStream ms = new MemoryStream(buffer);
            ////write to file
            //FileStream file = new FileStream("d:\\file.xls", FileMode.Create, FileAccess.Write);
            //ms.WriteTo(file);
            //file.Close();
            //ms.Close();

            DataSet ds = new DataSet();
            DataTable table = new DataTable("LoginDetails");
            string message = string.Empty;
            //try
            //{
            int RoleID = 0;
            int UserID = 0;
            int CompanyID = 0;
            string CompanyName = string.Empty;
            string firstname = string.Empty;
            string lastname = string.Empty;
            string emailid = string.Empty;
            string mobileno = string.Empty;
            int LocationID = 0;
            string LocationName = string.Empty;
            int GroupID = 0;
            int LanguageID = 0;
            string UserFName = string.Empty;
            string UserLName = string.Empty;
            string UserMailID = string.Empty;
            string ThemeFolderPath = string.Empty;



            if (!objSingleSignOn.Login(username, password, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
            {
                message = "Invalid User !";
                table.Columns.Add("status");
                table.Rows.Add("0");
            }
            else
            {
                table.Columns.Add("status");
                table.Columns.Add("userID");
                table.Columns.Add("CompanyID");
                table.Columns.Add("locationID");
                table.Columns.Add("companyName");
                table.Columns.Add("locationame");
                table.Columns.Add("firstname");
                table.Columns.Add("lastname");
                table.Columns.Add("emailid");
                table.Columns.Add("mobileno");
                table.Rows.Add(1, UserID, CompanyID, LocationID, CompanyName, LocationName, UserFName, UserLName, UserMailID);

            }
            //}
            //catch { }

            return ConvertJavaSeriptSerializer(table);
        }


        [WebMethod]
        public string getallsector(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                int id = 0;
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallsectorbyuserid");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Getcompanylogobyuserid(int companyid)
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                if (companyid != 0)
                {

                    Database db = DatabaseFactory.CreateDatabase("DBNameIs");
                    DbCommand cmd = db.GetStoredProcCommand("getlogobycompanyid");
                    db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                    ds = db.ExecuteDataSet(cmd);
                }



            }
            catch { }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetCriticalObservations()
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetAllCriticalObservations");
                ds = db.ExecuteDataSet(cmd);
            }
            catch { }
            return ConvertJavaSeriptSerializer_Dataset(ds);

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetFeedbackQuestions()
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetClientFeedbackQuestions");
                ds = db.ExecuteDataSet(cmd);
            }
            catch { }
            return ConvertJavaSeriptSerializer_Dataset(ds);

        }


        private string ConvertJavaSeriptSerializer_Dataset(DataSet dsCritOp)
        {
            Dictionary<string, List<object>> lstresultMain = new Dictionary<string, List<object>>();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            //List<object> lstresultMain = new List<object>();
            foreach (DataTable dt in dsCritOp.Tables)
            {
                List<object> resultMain = new List<object>();
                foreach (DataRow row in dt.Rows)
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    foreach (DataColumn column in dt.Columns)
                    {
                        result.Add(column.ColumnName, "" + row[column.ColumnName]);
                    }
                    resultMain.Add(result);
                }
                lstresultMain.Add(dt.Columns[0].ColumnName, resultMain);
            }
            return serializer.Serialize(lstresultMain);
        }

        private string ConvertJavaSeriptSerializer(DataTable dt)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            List<object> resultMain = new List<object>();
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                foreach (DataColumn column in dt.Columns)
                {
                    result.Add(column.ColumnName, "" + row[column.ColumnName]);
                }
                resultMain.Add(result);
            }
            return serializer.Serialize(resultMain);
        }

        [WebMethod]
        public string getallsbu(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_allsbu");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);

                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string GetCompanybyuserid(int userid)
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                if (userid != 0)
                {
                    //ds = objuserCompany.GetUserCompany(Convert.ToInt32(userid));

                    //DataTable dFiltered = ds.Tables[0].Clone();
                    //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
                    //foreach (DataRow dr in dRowUpdates)
                    //    dFiltered.ImportRow(dr);

                    //ds.Tables.Clear();
                    //ds.Tables.Add(dFiltered);
                    Database db = DatabaseFactory.CreateDatabase("DBNameIs");
                    DbCommand cmd = db.GetStoredProcCommand("sp_Getcompanybyuserid");
                    db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                    ds = db.ExecuteDataSet(cmd);
                    //DataTable dFiltered = ds.Tables[0].Clone();
                    //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
                    //foreach (DataRow dr in dRowUpdates)
                    //    dFiltered.ImportRow(dr);

                    //ds.Tables.Clear();
                    //ds.Tables.Add(dFiltered);

                }
            }
            catch { }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod]
        public string GetLocationsbyuserid(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (userid != 0)
                {

                    Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                    DbCommand cmd = db.GetStoredProcCommand("And_getlocationbyuserid");
                    db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                    ds = db.ExecuteDataSet(cmd);


                }
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);
        }

        [WebMethod]
        public string getmaudit(int sectorid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getmaudit");
                db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);


        }

        [WebMethod]
        public string getallaudit(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallmaudit");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);


                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }



        [WebMethod]
        public string getallcategory(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallcategory");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);


                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallquestion(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallquestion");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod]
        public string getallscore(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallscore");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }















        [WebMethod]
        public string gettransaction()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("gettransaction");
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);


        }


        #region "After Auto Sync"


        [WebMethod]
        public string getallauditNew(int userid, int type, string dtdate)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (dtdate.Trim() == string.Empty)
                {
                    return serializer.Serialize("Invalid Date");
                }
                DateTime dt = DateTime.ParseExact(dtdate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallmaudit_New");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "Type", DbType.Int32, type);
                db.AddInParameter(cmd, "Date", DbType.DateTime, dt);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallcategoryNew(int userid, int type, string dtdate)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (dtdate.Trim() == string.Empty)
                {
                    return serializer.Serialize("Invalid Date");
                }
                DateTime dt = DateTime.ParseExact(dtdate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallcategory_New");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "Type", DbType.Int32, type);
                db.AddInParameter(cmd, "Date", DbType.DateTime, dt);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallquestionNew(int userid, int type, string dtdate)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (dtdate.Trim() == string.Empty)
                {
                    return serializer.Serialize("Invalid Date");
                }
                DateTime dt = DateTime.ParseExact(dtdate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallquestion_New");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "Type", DbType.Int32, type);
                db.AddInParameter(cmd, "Date", DbType.DateTime, dt);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallscoreNew(int userid, int type, string dtdate)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (dtdate.Trim() == string.Empty)
                {
                    return serializer.Serialize("Invalid Date");
                }
                DateTime dt = DateTime.ParseExact(dtdate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallscore_New");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "Type", DbType.Int32, type);
                db.AddInParameter(cmd, "Date", DbType.DateTime, dt);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        #endregion


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public string InsertClientFeedback(string auditdate, int sbuid, int companyid, int locationid, string auditorname,
            string email, string designation, string mobile, int total, int scored, int scorepercentage, bool periodicalaudit, string suggestions,
            string clientsign, string transactionxml, string deviceguid, string clientname)
        {
            DataTable dtmsg = new DataTable();
            dtmsg.Columns.Add("Status", typeof(bool));
            dtmsg.Columns.Add("Message", typeof(string));
            dtmsg.Columns.Add("deviceguid", typeof(string));
            string _res = string.Empty;
            try
            {

                if (string.IsNullOrWhiteSpace(auditdate) || string.IsNullOrWhiteSpace(sbuid.ToString())
                     || string.IsNullOrWhiteSpace(companyid.ToString()) || string.IsNullOrWhiteSpace(locationid.ToString())
                    || string.IsNullOrWhiteSpace(total.ToString())
                    || string.IsNullOrWhiteSpace(scored.ToString())
                    || string.IsNullOrWhiteSpace(scorepercentage.ToString()))
                {
                    dtmsg.Rows.Add(false, "Invalid Inputs!!", deviceguid);
                    return ConvertJavaSeriptSerializer(dtmsg);
                }
                string clientguid = string.Empty;

                try
                {
                    byte[] clientbyte = null;
                    string bas64Image = clientsign;
                    if (bas64Image.Trim() != string.Empty)
                    {
                        clientbyte = Convert.FromBase64String(bas64Image);
                        if (clientsign.Length > 0)
                        {
                            clientguid = Guid.NewGuid().ToString();
                            ByteArrayToImage(clientbyte, ConfigurationManager.AppSettings["clientsignature"] + clientguid.Replace('/', ' '));
                            clientguid = clientguid + ".png";
                        }
                    }

                }
                catch
                {

                }

                DateTime dtAuditdate = Convert.ToDateTime(auditdate);
                string[] result = InsertClientFeedback(dtAuditdate, sbuid, companyid, locationid, auditorname,
             email, designation, mobile, total, scored, scorepercentage, periodicalaudit, suggestions,
             clientguid, transactionxml, deviceguid, clientname);
                if (result[1] != "-1")
                {
                    dtmsg.Rows.Add(true, result[0], deviceguid);
                    return ConvertJavaSeriptSerializer(dtmsg);
                }
                else
                {
                    dtmsg.Rows.Add(false, result[0], deviceguid);
                    return ConvertJavaSeriptSerializer(dtmsg);
                }
            }

            catch (Exception ex)
            {
                dtmsg.Rows.Add(false, ex.Message, deviceguid);
                return ConvertJavaSeriptSerializer(dtmsg);
            }

        }

        public string[] InsertClientFeedback(DateTime auditdate, int sbuid, int companyid, int locationid, string auditorname,
            string email, string designation, string mobile, int total, int scored, int scorepercentage, bool periodicalaudit, string suggestions,
            string clientsign, string transactionxml, string deviceguid, string clientname)
        {
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertClientFeedback");
            db.AddInParameter(cmd, "@Auditdate", DbType.DateTime, auditdate);
            db.AddInParameter(cmd, "@sbuid", DbType.Int32, sbuid);
            db.AddInParameter(cmd, "@CompanyId", DbType.Int32, companyid);
            db.AddInParameter(cmd, "@LocationId", DbType.Int32, locationid);
            db.AddInParameter(cmd, "@Auditorname", DbType.String, auditorname);
            db.AddInParameter(cmd, "@Email", DbType.String, email);
            db.AddInParameter(cmd, "@Designation", DbType.String, designation);

            db.AddInParameter(cmd, "@Mobileno", DbType.String, mobile);
            db.AddInParameter(cmd, "@Total", DbType.Int32, total);
            db.AddInParameter(cmd, "@Scored", DbType.Int32, scored);
            db.AddInParameter(cmd, "@ScorePercentage", DbType.Int32, scorepercentage);
            db.AddInParameter(cmd, "@periodicalAudit", DbType.Boolean, periodicalaudit);
            db.AddInParameter(cmd, "@Suggestions", DbType.String, suggestions);
            db.AddInParameter(cmd, "@clientsign", DbType.String, clientsign);
            db.AddInParameter(cmd, "@TransactionXML", DbType.String, transactionxml);

            db.AddInParameter(cmd, "@deviceguid", DbType.String, deviceguid);
            db.AddInParameter(cmd, "@clientname", DbType.String, clientname);

            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddOutParameter(cmd, "@Status", DbType.String, 10);
            db.ExecuteNonQuery(cmd);
            Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
            return Values;
        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string InsertTransaction(int sbuid, int companyid, int locationid, int sectorid, string XMLIs, string clientname, string sitename,
                                        string ssano, string clientperson, string sbuname, string aomname, string auditorname, string auditeename,
                                       string deviceID, string guiD, int userid, string auditdate, string auditorsignature, string clientsignature, string observation, string feedback,
            string Operations, string Training, string Scm, string Compliance, string hr, string Others, string followup, string totalScore, string towername)
        {

            // objbasebo.UpdateLog("XML : " + XMLIs);

            //XMLIs = "<root><facilityaudit  auditid='5' categoryid='59' questionid='404' scoreid='9' uploadfilename='' uploadfileGUID='7c7fb41d-5e38-483a-8180-7e267aa2f838' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='405' scoreid='9' uploadfilename='' uploadfileGUID='62f3f0b5-338a-4f46-be30-8c3fc8c94b69' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='406' scoreid='9' uploadfilename='' uploadfileGUID='fde519b1-4ea5-4d3f-8944-1bb234d3cb13' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='407' scoreid='9' uploadfilename='' uploadfileGUID='0070b44e-fea8-4242-9b36-72599dff3cf3' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='408' scoreid='9' uploadfilename='' uploadfileGUID='5b42a340-7eaf-4256-8f2c-26c0f5462529' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='409' scoreid='9' uploadfilename='' uploadfileGUID='164db7d0-d1d5-43f7-8a85-0edc46143fc4' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='410' scoreid='9' uploadfilename='' uploadfileGUID='932a8709-3679-4ed5-85c3-859cf8e93676' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='411' scoreid='9' uploadfilename='' uploadfileGUID='4b311b75-145d-45e0-a07f-aaaf497e3ede' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='412' scoreid='9' uploadfilename='' uploadfileGUID='f6cffbf6-35da-4afb-b7c4-7ebde247012c' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='413' scoreid='9' uploadfilename='' uploadfileGUID='a67c7a80-8323-42bc-93f4-70412385a4d8' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='414' scoreid='9' uploadfilename='' uploadfileGUID='14b2a219-9d9e-439e-bdc8-1f6ad691f6db' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='415' scoreid='9' uploadfilename='' uploadfileGUID='50021e9d-9610-4dbf-80f0-9754aead1e4e' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='416' scoreid='9' uploadfilename='' uploadfileGUID='966baad7-5578-43cd-af2d-17d72f9cb3f7' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='417' scoreid='9' uploadfilename='' uploadfileGUID='2e14f727-de4f-48d3-8178-83d05d333ac4' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='418' scoreid='9' uploadfilename='' uploadfileGUID='1c9a9f88-a75d-4527-be09-118b53101951' remarks='' />,<facilityaudit  auditid='5' categoryid='59' questionid='419' scoreid='9' uploadfilename='' uploadfileGUID='1f43f157-8e2a-4061-bd1d-66304dadc624' remarks='' />,<facilityaudit  auditid='5' categoryid='60' questionid='420' scoreid='7' uploadfilename='' uploadfileGUID='e38fdf42-1bb7-4929-b9b4-d2172ddfe9db' remarks='' />,<facilityaudit  auditid='5' categoryid='60' questionid='421' scoreid='7' uploadfilename='' uploadfileGUID='1e4e7fcf-5321-4908-bde3-36f3a88c2939' remarks='' />,<facilityaudit  auditid='5' categoryid='60' questionid='422' scoreid='7' uploadfilename='' uploadfileGUID='5ab18ffb-fe89-4cdf-a9fc-fa79a2a1e7da' remarks='' />,<facilityaudit  auditid='5' categoryid='60' questionid='423' scoreid='8' uploadfilename='' uploadfileGUID='c6a3b8a0-1cbc-4e7b-8d96-4aac15378bef' remarks='' />,<facilityaudit  auditid='5' categoryid='60' questionid='424' scoreid='7' uploadfilename='' uploadfileGUID='631e4df0-2cdf-4647-b212-65fbb1e34297' remarks='' />,<facilityaudit  auditid='5' categoryid='60' questionid='425' scoreid='7' uploadfilename='' uploadfileGUID='365c4679-0566-4284-956a-228529434992' remarks='' />,<facilityaudit  auditid='5' categoryid='60' questionid='426' scoreid='7' uploadfilename='' uploadfileGUID='184fa178-0774-4d20-9055-2b557bf8429c' remarks='' />,<facilityaudit  auditid='5' categoryid='61' questionid='427' scoreid='9' uploadfilename='' uploadfileGUID='784f13af-bb81-434b-b8dc-3707e18aa951' remarks='' />,<facilityaudit  auditid='5' categoryid='61' questionid='428' scoreid='9' uploadfilename='' uploadfileGUID='eece4638-2586-44f3-b4d8-8353206a0120' remarks='' />,<facilityaudit  auditid='5' categoryid='61' questionid='429' scoreid='9' uploadfilename='' uploadfileGUID='b14cec41-0202-43dc-a755-18d8807f98a7' remarks='' />,<facilityaudit  auditid='5' categoryid='61' questionid='430' scoreid='9' uploadfilename='' uploadfileGUID='261bc98a-ce3b-41b8-82a4-98ba27a55b93' remarks='' />,<facilityaudit  auditid='5' categoryid='61' questionid='431' scoreid='9' uploadfilename='' uploadfileGUID='c615cfb1-bf1a-4205-9837-d7b2b133d9da' remarks='' />,<facilityaudit  auditid='5' categoryid='61' questionid='432' scoreid='9' uploadfilename='' uploadfileGUID='aede635b-8637-4303-b71c-34f1e81ea370' remarks='' />,<facilityaudit  auditid='5' categoryid='62' questionid='433' scoreid='9' uploadfilename='' uploadfileGUID='2853eb8f-6585-4584-bf4f-c646d0a5c5d9' remarks='' />,<facilityaudit  auditid='5' categoryid='62' questionid='434' scoreid='9' uploadfilename='' uploadfileGUID='998f0765-7ade-482a-84b2-8a0f9493b33b' remarks='' />,<facilityaudit  auditid='5' categoryid='62' questionid='435' scoreid='9' uploadfilename='' uploadfileGUID='d3d90ca4-a0bf-43e5-9312-8e803f3542fa' remarks='' />,<facilityaudit  auditid='5' categoryid='62' questionid='436' scoreid='9' uploadfilename='' uploadfileGUID='35c19b9c-aabf-4a34-9545-53b6286895af' remarks='' />,<facilityaudit  auditid='5' categoryid='62' questionid='437' scoreid='9' uploadfilename='' uploadfileGUID='c6f96895-9035-4488-accb-53b4000d36a2' remarks='' />,<facilityaudit  auditid='5' categoryid='62' questionid='438' scoreid='9' uploadfilename='' uploadfileGUID='a2ce0a4c-3258-424d-bd68-dfa387140f53' remarks='' />,<facilityaudit  auditid='5' categoryid='62' questionid='439' scoreid='9' uploadfilename='' uploadfileGUID='756e6761-1b3d-4752-83b3-7b672887fd64' remarks='' />,<facilityaudit  auditid='5' categoryid='62' questionid='440' scoreid='9' uploadfilename='' uploadfileGUID='46ab1c49-33a0-4b9c-bfdf-9ce1710234db' remarks='' />,<facilityaudit  auditid='5' categoryid='63' questionid='441' scoreid='9' uploadfilename='' uploadfileGUID='b43b5c3d-0650-459f-ba6f-3c2529cc3a16' remarks='' />,<facilityaudit  auditid='5' categoryid='63' questionid='442' scoreid='9' uploadfilename='' uploadfileGUID='5f055863-76a9-4ddc-9dd2-f6d8231c07e1' remarks='' />,<facilityaudit  auditid='5' categoryid='63' questionid='443' scoreid='9' uploadfilename='' uploadfileGUID='58d45353-788c-43dc-893a-c493201fdd82' remarks='' />,<facilityaudit  auditid='5' categoryid='64' questionid='444' scoreid='9' uploadfilename='' uploadfileGUID='d6d0ca99-2bf7-443c-9421-25f44c06cffc' remarks='' />,<facilityaudit  auditid='5' categoryid='64' questionid='445' scoreid='9' uploadfilename='' uploadfileGUID='07f5bf22-0e66-4662-8824-d6a987e6b3e2' remarks='' />,<facilityaudit  auditid='5' categoryid='64' questionid='446' scoreid='9' uploadfilename='' uploadfileGUID='1b7dd38e-b407-435c-92d6-689fbed8f961' remarks='' />,<facilityaudit  auditid='5' categoryid='64' questionid='447' scoreid='9' uploadfilename='' uploadfileGUID='e917f742-3fd0-4464-a37d-8f87d89d3ada' remarks='' />,<facilityaudit  auditid='5' categoryid='65' questionid='448' scoreid='9' uploadfilename='' uploadfileGUID='c05e44ea-c0fb-4759-8de8-64f0e7f9e302' remarks='' />,<facilityaudit  auditid='5' categoryid='65' questionid='449' scoreid='9' uploadfilename='' uploadfileGUID='f8411ed6-fcfe-40de-8017-698158860a17' remarks='' />,<facilityaudit  auditid='5' categoryid='66' questionid='450' scoreid='9' uploadfilename='' uploadfileGUID='eaf6a3c3-33c9-451b-bafc-903867097a45' remarks='' />,<facilityaudit  auditid='5' categoryid='66' questionid='451' scoreid='9' uploadfilename='' uploadfileGUID='c191069e-e302-4fac-8c94-01217517e70e' remarks='' />,<facilityaudit  auditid='5' categoryid='66' questionid='452' scoreid='9' uploadfilename='' uploadfileGUID='ba73e19b-9593-4c85-8b1c-510cc1f48e00' remarks='' />,<facilityaudit  auditid='5' categoryid='66' questionid='453' scoreid='9' uploadfilename='' uploadfileGUID='837fa6cf-fe3d-4b05-8198-dc571ffb72b2' remarks='' />,<facilityaudit  auditid='5' categoryid='67' questionid='454' scoreid='7' uploadfilename='' uploadfileGUID='3cbfcc8c-b316-4b50-8818-6a8d45c41b8a' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='455' scoreid='37' uploadfilename='' uploadfileGUID='a6ccd3de-4b51-41da-8c65-3e9aaf4f4e71' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='456' scoreid='37' uploadfilename='' uploadfileGUID='6f1816dd-1673-4499-b9db-ddcdcd8f6daf' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='457' scoreid='38' uploadfilename='' uploadfileGUID='a894ede6-f218-4bd3-930a-13576affde87' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='458' scoreid='37' uploadfilename='' uploadfileGUID='c6c6a153-28dd-495d-8509-c2e2e89ccbe3' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='459' scoreid='38' uploadfilename='' uploadfileGUID='90eafabc-ed5b-4d18-8f5c-257497fbc216' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='460' scoreid='37' uploadfilename='' uploadfileGUID='af742e8d-db16-4120-97a6-920f53b7b159' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='461' scoreid='38' uploadfilename='' uploadfileGUID='840fb55d-36be-4809-ae35-6dd53ead3ae4' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='462' scoreid='37' uploadfilename='' uploadfileGUID='9b5105b4-100f-48ae-bf25-eefe29b8c55c' remarks='' />,<facilityaudit  auditid='6' categoryid='68' questionid='463' scoreid='37' uploadfilename='' uploadfileGUID='8bc522e8-9645-4363-a16e-f217570ace15' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='464' scoreid='39' uploadfilename='' uploadfileGUID='442424f2-a2d1-419a-8b2a-7507597f7abc' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='465' scoreid='39' uploadfilename='' uploadfileGUID='770d33d5-31a9-47fc-9479-dd7bbc71aa68' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='466' scoreid='39' uploadfilename='' uploadfileGUID='8672f74c-3aa0-4e5e-b814-07a446b55b2f' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='467' scoreid='39' uploadfilename='' uploadfileGUID='f0d97300-67b7-4b4e-8300-b4054c95d6c3' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='468' scoreid='39' uploadfilename='' uploadfileGUID='9473f5af-68e9-45bb-af25-ffcc1ece4ebe' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='469' scoreid='39' uploadfilename='' uploadfileGUID='6599d06a-8c3a-425e-869f-cae09e21fdd5' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='470' scoreid='39' uploadfilename='' uploadfileGUID='602c4374-1734-4eeb-86fc-d6ff929dfb52' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='471' scoreid='39' uploadfilename='' uploadfileGUID='78ad9a10-7311-4c56-af05-7c9d5125af73' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='472' scoreid='39' uploadfilename='' uploadfileGUID='d3cbadb0-95ea-4335-b822-70196e0b7615' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='473' scoreid='39' uploadfilename='' uploadfileGUID='bdf4fd69-8bf2-4f1f-b852-3b887e9a2c12' remarks='' />,<facilityaudit  auditid='6' categoryid='69' questionid='474' scoreid='39' uploadfilename='' uploadfileGUID='0a991a9c-22e7-4f89-bffa-c471f40565ea' remarks='' />,<facilityaudit  auditid='6' categoryid='70' questionid='475' scoreid='39' uploadfilename='' uploadfileGUID='8029f07e-fe4b-4216-8992-199b299f224c' remarks='' />,<facilityaudit  auditid='6' categoryid='70' questionid='476' scoreid='39' uploadfilename='' uploadfileGUID='00609df3-7577-4f57-b931-4b232c89b988' remarks='' />,<facilityaudit  auditid='6' categoryid='70' questionid='477' scoreid='39' uploadfilename='' uploadfileGUID='9972a910-fa90-40d0-a8e3-f4476f8cfedc' remarks='' />,<facilityaudit  auditid='6' categoryid='70' questionid='478' scoreid='39' uploadfilename='' uploadfileGUID='9bf35e03-a216-4703-8b71-6b2d19ad2400' remarks='' />,<facilityaudit  auditid='6' categoryid='70' questionid='479' scoreid='39' uploadfilename='' uploadfileGUID='3515ceaa-35cd-4879-a44b-e1c90cb590fb' remarks='' />,<facilityaudit  auditid='6' categoryid='70' questionid='480' scoreid='39' uploadfilename='' uploadfileGUID='33d921bd-d515-4521-bbeb-8ad84ed399c4' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='481' scoreid='37' uploadfilename='' uploadfileGUID='a5e7242d-788e-4dd7-8fc3-591e03cdd1ac' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='482' scoreid='37' uploadfilename='' uploadfileGUID='174ca86e-8fdb-4fde-95e2-2187b4add747' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='483' scoreid='37' uploadfilename='' uploadfileGUID='3f06bd97-9469-40ae-b71f-345e09a0e3d4' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='484' scoreid='38' uploadfilename='' uploadfileGUID='3873e1e8-a834-4697-967a-09d1f101b9a8' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='485' scoreid='37' uploadfilename='' uploadfileGUID='ede16351-8420-4e8d-b604-01e85656c39c' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='486' scoreid='38' uploadfilename='' uploadfileGUID='e47f1d12-9539-4059-a3f7-007ce4ebce92' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='487' scoreid='37' uploadfilename='' uploadfileGUID='73af2ec5-d2bf-4e1e-9482-7322ed69a7b1' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='488' scoreid='37' uploadfilename='' uploadfileGUID='93af5be7-c94e-43e1-b621-ba972f70b39a' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='489' scoreid='37' uploadfilename='' uploadfileGUID='428de10c-e368-4f65-9936-363c07de5c5b' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='490' scoreid='37' uploadfilename='' uploadfileGUID='29b43fa6-3307-4375-83c8-65db500094c4' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='491' scoreid='37' uploadfilename='' uploadfileGUID='74e9207a-48a2-44bf-927b-22d8ef57f1fd' remarks='' />,<facilityaudit  auditid='6' categoryid='71' questionid='492' scoreid='37' uploadfilename='' uploadfileGUID='60b807bf-426e-4469-823c-a4684e4a2ce2' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='493' scoreid='37' uploadfilename='' uploadfileGUID='1ff0dd2f-1f95-4801-8e12-63cab9759557' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='494' scoreid='37' uploadfilename='' uploadfileGUID='0853e847-c79f-4504-bd05-ec4fe758f582' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='495' scoreid='38' uploadfilename='' uploadfileGUID='80bb5cf6-cc00-4bb1-90d4-74d9bb4f3346' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='496' scoreid='37' uploadfilename='' uploadfileGUID='8a61a89b-502b-4d32-bc49-76aaac082b5a' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='497' scoreid='38' uploadfilename='' uploadfileGUID='ab4a09c0-b721-423b-b2d5-03eb663b0297' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='498' scoreid='37' uploadfilename='' uploadfileGUID='1169182f-600a-4cbd-a390-6e82910d4e36' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='499' scoreid='37' uploadfilename='' uploadfileGUID='09a1524d-43c2-4843-ad8b-31cc9bd7ebcc' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='500' scoreid='38' uploadfilename='' uploadfileGUID='802d6eb0-c277-4ce1-b2c4-1c47315597e5' remarks='' />,<facilityaudit  auditid='6' categoryid='72' questionid='501' scoreid='37' uploadfilename='' uploadfileGUID='cf7601f9-1349-4eda-866c-7dd53ecb6625' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='502' scoreid='39' uploadfilename='' uploadfileGUID='3a3d60c7-4dcc-4fac-84ea-72111d39a584' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='503' scoreid='39' uploadfilename='' uploadfileGUID='af22ca51-bfc2-4ebd-8091-894dde58382e' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='504' scoreid='39' uploadfilename='' uploadfileGUID='a984615e-0d29-46b3-84d1-156f4fdb7632' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='505' scoreid='39' uploadfilename='' uploadfileGUID='531de981-b5d8-4b7c-b22b-afd026fbc261' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='506' scoreid='39' uploadfilename='' uploadfileGUID='51f71874-802f-4f3b-9215-0c86ae4c9268' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='507' scoreid='39' uploadfilename='' uploadfileGUID='ba7117b6-d823-402b-8818-0355dd93fe44' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='508' scoreid='39' uploadfilename='' uploadfileGUID='cc314966-a87f-4480-a26d-649ae556f584' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='509' scoreid='39' uploadfilename='' uploadfileGUID='f179436f-420a-4d5f-a654-075aeac6f226' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='510' scoreid='39' uploadfilename='' uploadfileGUID='8ddbe071-e358-4fe7-b91b-8deed7459ff1' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='511' scoreid='39' uploadfilename='' uploadfileGUID='ef4dfec7-7c46-4312-9c3d-00c831345a89' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='512' scoreid='39' uploadfilename='' uploadfileGUID='2752ba2b-a391-4727-b911-3bd0c655d969' remarks='' />,<facilityaudit  auditid='6' categoryid='73' questionid='513' scoreid='39' uploadfilename='' uploadfileGUID='6567e6ce-896d-40ab-98b9-f0aab582ff49' remarks='' />,<facilityaudit  auditid='6' categoryid='74' questionid='514' scoreid='37' uploadfilename='' uploadfileGUID='e8034da4-e43d-4c7a-81b7-3c6c7cfe1ce3' remarks='' />,<facilityaudit  auditid='6' categoryid='74' questionid='515' scoreid='38' uploadfilename='' uploadfileGUID='e303d382-c235-47f0-bc8d-d2ede29b4f43' remarks='' />,<facilityaudit  auditid='6' categoryid='74' questionid='516' scoreid='37' uploadfilename='' uploadfileGUID='bfd232e5-66e0-49ba-8e8d-ebafbf623da6' remarks='' />,<facilityaudit  auditid='6' categoryid='75' questionid='517' scoreid='39' uploadfilename='' uploadfileGUID='4f980e81-1995-4c9b-99c2-9a776e453c4e' remarks='' />,<facilityaudit  auditid='6' categoryid='75' questionid='518' scoreid='39' uploadfilename='' uploadfileGUID='ec14732a-e4e6-4c86-a603-6711179de58c' remarks='' />,<facilityaudit  auditid='6' categoryid='75' questionid='519' scoreid='39' uploadfilename='' uploadfileGUID='a53c15f3-7e64-49c6-a354-34379056c882' remarks='' />,<facilityaudit  auditid='6' categoryid='75' questionid='520' scoreid='39' uploadfilename='' uploadfileGUID='a6cb8c00-98d1-4a48-97cb-54d9e302b553' remarks='' />,<facilityaudit  auditid='6' categoryid='76' questionid='521' scoreid='37' uploadfilename='' uploadfileGUID='9c4dd28b-68b1-4409-816f-f750b6f74d76' remarks='' />,<facilityaudit  auditid='6' categoryid='76' questionid='522' scoreid='38' uploadfilename='' uploadfileGUID='c1c0450f-ce40-4c13-89d5-1f3b2942a53d' remarks='' />,<facilityaudit  auditid='6' categoryid='76' questionid='523' scoreid='37' uploadfilename='' uploadfileGUID='196d10a6-a4e2-49df-8fdf-fcb087783254' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='524' scoreid='37' uploadfilename='' uploadfileGUID='001727bc-8bec-48f5-9523-8033c737e551' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='525' scoreid='37' uploadfilename='' uploadfileGUID='194f0c43-08ef-4d66-8a93-c58a7e02d109' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='526' scoreid='37' uploadfilename='' uploadfileGUID='08112c29-b458-45a8-89f8-88efec3cd90a' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='527' scoreid='37' uploadfilename='' uploadfileGUID='b8f09f47-3ba2-4cc4-8146-47bd4c9c0cce' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='528' scoreid='37' uploadfilename='' uploadfileGUID='be5f05ee-ecfb-48d2-82fa-b3a396dabe3e' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='529' scoreid='37' uploadfilename='' uploadfileGUID='ffe8cd88-e923-4cf3-b550-8e56abd1b11b' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='530' scoreid='37' uploadfilename='' uploadfileGUID='4bb4a00c-8b2a-4c31-930d-256a1f810133' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='531' scoreid='37' uploadfilename='' uploadfileGUID='07670f76-1a26-4742-a714-abbd65462acc' remarks='' />,<facilityaudit  auditid='6' categoryid='77' questionid='532' scoreid='37' uploadfilename='' uploadfileGUID='af907f30-c4ea-4e89-8362-d895a01d655c' remarks='' />,<facilityaudit  auditid='6' categoryid='78' questionid='533' scoreid='37' uploadfilename='' uploadfileGUID='83e0803d-35ba-4380-8ec7-98857c6539eb' remarks='' />,<facilityaudit  auditid='6' categoryid='78' questionid='534' scoreid='38' uploadfilename='' uploadfileGUID='01ca3783-8b61-4d53-a0e4-a8d9c6d8f444' remarks='' />,<facilityaudit  auditid='6' categoryid='78' questionid='535' scoreid='37' uploadfilename='' uploadfileGUID='41d1846d-d598-4b85-b506-063dd390ad75' remarks='' />,<facilityaudit  auditid='6' categoryid='79' questionid='536' scoreid='37' uploadfilename='' uploadfileGUID='a4ea8c42-2a53-4850-bebe-761794b7ae34' remarks='' />,<facilityaudit  auditid='6' categoryid='79' questionid='537' scoreid='38' uploadfilename='' uploadfileGUID='7df85abe-cde9-4242-96fa-a8a2c2947f96' remarks='' />,<facilityaudit  auditid='6' categoryid='79' questionid='538' scoreid='37' uploadfilename='' uploadfileGUID='0a26917f-49f3-4f6d-b1b3-5e49c56f9600' remarks='' />,<facilityaudit  auditid='6' categoryid='79' questionid='539' scoreid='38' uploadfilename='' uploadfileGUID='63ba8e6c-d67a-463d-b254-712c5a7244da' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='540' scoreid='37' uploadfilename='' uploadfileGUID='7a612812-7ae6-4d26-b95a-31b7a6777495' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='541' scoreid='37' uploadfilename='' uploadfileGUID='af247bda-8343-4499-9e2c-35c547b1e04d' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='542' scoreid='38' uploadfilename='' uploadfileGUID='ada38bb2-095b-4933-a75b-ef7b53c8f596' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='543' scoreid='37' uploadfilename='' uploadfileGUID='b6dc5297-e066-4deb-9444-59311e23e6dc' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='544' scoreid='37' uploadfilename='' uploadfileGUID='fafa984b-2018-46f1-9d9f-7ee8285c40b7' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='545' scoreid='37' uploadfilename='' uploadfileGUID='59a41f2c-7047-4bff-ac0d-3d12e1582edd' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='546' scoreid='37' uploadfilename='' uploadfileGUID='a83f78f1-39a2-46e5-b62a-cefd709f307e' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='547' scoreid='37' uploadfilename='' uploadfileGUID='d14bf134-5f74-4518-925c-6e0db98f5fb1' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='548' scoreid='37' uploadfilename='' uploadfileGUID='a53c3707-1fde-40ba-a64a-09514181dbd5' remarks='' />,<facilityaudit  auditid='6' categoryid='80' questionid='549' scoreid='37' uploadfilename='' uploadfileGUID='0a3f1dae-98b8-4c6b-96ef-b0f272ac886d' remarks='' />,<facilityaudit  auditid='6' categoryid='81' questionid='550' scoreid='37' uploadfilename='' uploadfileGUID='373d72f0-a68a-452c-a0b4-4962d2bda021' remarks='' />,<facilityaudit  auditid='6' categoryid='81' questionid='551' scoreid='37' uploadfilename='' uploadfileGUID='1cb28782-bb86-42da-83aa-a407345967e2' remarks='' />,<facilityaudit  auditid='6' categoryid='81' questionid='552' scoreid='37' uploadfilename='' uploadfileGUID='62e87e5e-e7b8-4ffc-9ca2-9e3b109f7731' remarks='' />,<facilityaudit  auditid='6' categoryid='81' questionid='553' scoreid='37' uploadfilename='' uploadfileGUID='409b232e-cab2-48eb-a7af-3a8eee3d43b1' remarks='' />,<facilityaudit  auditid='6' categoryid='81' questionid='554' scoreid='38' uploadfilename='' uploadfileGUID='abe7ad0f-b74b-4a7d-aed5-ff0c5b6eed50' remarks='' />,<facilityaudit  auditid='6' categoryid='81' questionid='555' scoreid='37' uploadfilename='' uploadfileGUID='dae4f74a-90e1-4188-999c-c325a60c0c9c' remarks='' />,<facilityaudit  auditid='6' categoryid='82' questionid='556' scoreid='37' uploadfilename='' uploadfileGUID='1f6a0494-4930-4373-9669-e7056bb09b74' remarks='' />,<facilityaudit  auditid='6' categoryid='82' questionid='557' scoreid='38' uploadfilename='' uploadfileGUID='c514f749-8d75-4e89-ac51-d9961536201c' remarks='' />,<facilityaudit  auditid='6' categoryid='82' questionid='558' scoreid='37' uploadfilename='' uploadfileGUID='a6f99670-7c23-4c5d-adcc-851f816f7fac' remarks='' />,<facilityaudit  auditid='6' categoryid='82' questionid='559' scoreid='37' uploadfilename='' uploadfileGUID='9e0808c1-c170-4052-beee-2989a07a3d5b' remarks='' />,<facilityaudit  auditid='6' categoryid='83' questionid='560' scoreid='37' uploadfilename='' uploadfileGUID='ba0af236-d4ad-402d-9973-f0dd4ab27462' remarks='' />,<facilityaudit  auditid='6' categoryid='83' questionid='561' scoreid='37' uploadfilename='' uploadfileGUID='910d7941-6039-4fc4-a584-06a048dc6857' remarks='' />,<facilityaudit  auditid='6' categoryid='83' questionid='562' scoreid='37' uploadfilename='' uploadfileGUID='c2cee4f1-34d3-41d0-bf7b-c185ce0506c1' remarks='' />,<facilityaudit  auditid='6' categoryid='83' questionid='563' scoreid='37' uploadfilename='' uploadfileGUID='3cb875c8-f4e8-4047-9896-e0a96319e358' remarks='' />,<facilityaudit  auditid='6' categoryid='83' questionid='564' scoreid='37' uploadfilename='' uploadfileGUID='4f614ac9-70c5-4339-a00c-e64a3e526406' remarks='' />,<facilityaudit  auditid='6' categoryid='84' questionid='565' scoreid='37' uploadfilename='' uploadfileGUID='e4c03a68-2578-4060-b97e-7f2fbed41203' remarks='' />,<facilityaudit  auditid='6' categoryid='84' questionid='566' scoreid='37' uploadfilename='' uploadfileGUID='e5081eea-4d14-4050-80cd-e67e1582634f' remarks='' />,<facilityaudit  auditid='6' categoryid='84' questionid='567' scoreid='37' uploadfilename='' uploadfileGUID='853faab1-06bf-4efb-bc3b-d85a8c121ddf' remarks='' />,<facilityaudit  auditid='6' categoryid='84' questionid='568' scoreid='37' uploadfilename='' uploadfileGUID='24ea9490-8ab5-4e40-a1be-7180fd5eef31' remarks='' />,<facilityaudit  auditid='6' categoryid='84' questionid='569' scoreid='37' uploadfilename='' uploadfileGUID='d8813319-3dda-427c-90fe-a77cff230db5' remarks='' />,<facilityaudit  auditid='6' categoryid='84' questionid='570' scoreid='38' uploadfilename='' uploadfileGUID='62ef4bc6-a66f-4b08-beab-418d9ef5ed8d' remarks='' />,<facilityaudit  auditid='6' categoryid='84' questionid='571' scoreid='37' uploadfilename='' uploadfileGUID='f923e672-516c-43ec-8be2-adb53ad2ee6b' remarks='' />,<facilityaudit  auditid='6' categoryid='84' questionid='572' scoreid='37' uploadfilename='' uploadfileGUID='bb6f4241-d5d0-48e4-9290-3809934095b8' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='573' scoreid='39' uploadfilename='' uploadfileGUID='2cd00734-120c-4654-9281-9c10484ed1bf' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='574' scoreid='39' uploadfilename='' uploadfileGUID='348581dc-b087-49c5-ad60-ef8e555c82b6' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='575' scoreid='39' uploadfilename='' uploadfileGUID='bd2ac00f-8faf-4ce4-9a1e-84ffc7d89a07' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='576' scoreid='39' uploadfilename='' uploadfileGUID='4f3cd06c-8595-4896-9e57-36d7a92047ff' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='577' scoreid='39' uploadfilename='' uploadfileGUID='c5805eb2-157f-4cf7-9438-142368463819' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='578' scoreid='39' uploadfilename='' uploadfileGUID='4c5b6edb-b87f-4560-85e1-330ab97b8bad' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='579' scoreid='39' uploadfilename='' uploadfileGUID='5939267b-7d9d-475b-866e-51cd276d8065' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='580' scoreid='39' uploadfilename='' uploadfileGUID='5d42e333-94f6-437f-8356-f25b5efa8d5d' remarks='' />,<facilityaudit  auditid='6' categoryid='85' questionid='581' scoreid='39' uploadfilename='' uploadfileGUID='863a1dd8-9d2e-4926-9543-f9d438b02139' remarks='' />,<facilityaudit  auditid='6' categoryid='86' questionid='582' scoreid='39' uploadfilename='' uploadfileGUID='d9fe9eb8-0b78-4b88-a6d7-7d67771d1d04' remarks='' />,<facilityaudit  auditid='6' categoryid='86' questionid='583' scoreid='39' uploadfilename='' uploadfileGUID='8414a277-e425-438a-97a4-7d710b12b416' remarks='' />,<facilityaudit  auditid='6' categoryid='86' questionid='584' scoreid='39' uploadfilename='' uploadfileGUID='a25e74d8-ea74-4f93-9f61-6a622e031cca' remarks='' />,<facilityaudit  auditid='6' categoryid='86' questionid='585' scoreid='39' uploadfilename='' uploadfileGUID='c6486242-5694-4cbd-a798-b675eb00a0f7' remarks='' />,<facilityaudit  auditid='6' categoryid='86' questionid='586' scoreid='39' uploadfilename='' uploadfileGUID='472522c9-9c13-4df3-a90f-4a372ddb5058' remarks='' />,<facilityaudit  auditid='6' categoryid='87' questionid='587' scoreid='39' uploadfilename='' uploadfileGUID='f85ec76a-feee-44c0-abf0-47c12f2dca01' remarks='' />,<facilityaudit  auditid='6' categoryid='87' questionid='588' scoreid='39' uploadfilename='' uploadfileGUID='26bad8da-15a9-4329-a712-86ca53db8037' remarks='' />,<facilityaudit  auditid='6' categoryid='87' questionid='589' scoreid='39' uploadfilename='' uploadfileGUID='c9526252-b1a0-40c6-a5db-4f21c33a4ac7' remarks='' />,<facilityaudit  auditid='6' categoryid='87' questionid='590' scoreid='39' uploadfilename='' uploadfileGUID='af264d7f-eda7-4e46-975b-b2033f2284ec' remarks='' />,<facilityaudit  auditid='6' categoryid='87' questionid='591' scoreid='39' uploadfilename='' uploadfileGUID='a123e088-373b-4e59-8a95-d4a6de3e4b84' remarks='' />,<facilityaudit  auditid='6' categoryid='87' questionid='592' scoreid='39' uploadfilename='' uploadfileGUID='1183462f-5c20-4b23-9002-7efbbb385e23' remarks='' />,<facilityaudit  auditid='6' categoryid='87' questionid='593' scoreid='39' uploadfilename='' uploadfileGUID='7c4725bd-d406-416c-9b27-a1d484dca8c3' remarks='' />,<facilityaudit  auditid='6' categoryid='87' questionid='594' scoreid='39' uploadfilename='' uploadfileGUID='a22b4ab8-19b8-4827-9a8a-51333a8eaf74' remarks='' />,<facilityaudit  auditid='6' categoryid='88' questionid='595' scoreid='37' uploadfilename='' uploadfileGUID='b10b640a-99aa-4de6-9b2e-794fa077b6de' remarks='' />,<facilityaudit  auditid='6' categoryid='88' questionid='596' scoreid='37' uploadfilename='' uploadfileGUID='49028ca8-d14d-4a66-a9fc-1ab1025e40b1' remarks='' />,<facilityaudit  auditid='6' categoryid='88' questionid='597' scoreid='37' uploadfilename='' uploadfileGUID='42c4b5df-59bc-4fca-9224-0acbc7b557d8' remarks='' />,<facilityaudit  auditid='6' categoryid='88' questionid='598' scoreid='37' uploadfilename='' uploadfileGUID='d389ee7a-62e8-4cff-91a2-7f88bed823be' remarks='' />,<facilityaudit  auditid='6' categoryid='88' questionid='599' scoreid='38' uploadfilename='' uploadfileGUID='339a90a1-8bad-425b-bc8b-4232363815cd' remarks='' />,<facilityaudit  auditid='6' categoryid='88' questionid='600' scoreid='37' uploadfilename='' uploadfileGUID='53aaefd0-89bf-4717-800d-d387cd1032ed' remarks='' /></root>";

            DataSet ds2 = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("CheckAduittransaction");
            db.AddInParameter(cmd, "deviceID", DbType.String, deviceID);
            db.AddInParameter(cmd, "guiD", DbType.String, guiD);
            ds2 = db.ExecuteDataSet(cmd);


            if (ds2 != null && ds2.Tables.Count > 0 && ds2.Tables[0].Rows.Count > 0)
            {
                return ConvertJavaSeriptSerializer(ds2.Tables[0]);
            }
            else
            {

                #region "New"

                try
                {
                    DateTime auddate = Convert.ToDateTime(auditdate);

                    DbCommand command = db.GetStoredProcCommand("SP_InsertAuditHistory");
                    db.AddInParameter(command, "locationid", DbType.Int32, locationid);
                    db.AddInParameter(command, "towername", DbType.String, towername);
                    db.AddInParameter(command, "xmlstring", DbType.String, XMLIs);
                    db.AddInParameter(command, "Auditdate", DbType.DateTime, auddate);
                    db.ExecuteNonQuery(command);

                    //DataSet dsTransaction = new DataSet();
                    //DbCommand cmdTransaction = db.GetStoredProcCommand("Qry_CheckTransaction");
                    //db.AddInParameter(cmdTransaction, "@LocationId", DbType.Int32, locationid);
                    //db.AddInParameter(cmdTransaction, "@AuditDate", DbType.DateTime, auddate);
                    //dsTransaction = db.ExecuteDataSet(cmdTransaction);
                    //if (dsTransaction != null && dsTransaction.Tables.Count > 0 && dsTransaction.Tables[0].Rows.Count > 0)
                    //{
                    //    return ConvertJavaSeriptSerializer(dsTransaction.Tables[0]);
                    //}

                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    DataSet dtinspect = new DataSet();
                    DataTable dlinspect = new DataTable();
                    StringBuilder objbuilder = new StringBuilder();

                    //"21:09:2015 13:54:12 PM"
                    //string[] fdate = XMLIs.Split(':');
                    //XMLIs = fdate[1].ToString() + "/" + fdate[0].ToString() + "/" + fdate[2].ToString() + ":" + fdate[3].ToString() + ":" + fdate[4].ToString();

                    byte[] auditorsign = null;
                    byte[] clientsign = null;
                    string auditorguid = "";
                    string clientguid = "";
                    try
                    {
                        string bas64Image = auditorsignature;
                        if (bas64Image.Trim() != string.Empty)
                        {
                            auditorsign = Convert.FromBase64String(bas64Image);
                            if (auditorsign.Length > 0)
                            {
                                auditorguid = Guid.NewGuid().ToString();
                                ByteArrayToImage(auditorsign, ConfigurationManager.AppSettings["auditorsignature"] + auditorguid.Replace('/', ' '));
                                auditorguid = auditorguid + ".png";
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        return serializer.Serialize(e.Message);
                    }

                    try
                    {
                        string bas64Image = clientsignature;
                        if (bas64Image.Trim() != string.Empty)
                        {
                            clientsign = Convert.FromBase64String(bas64Image);
                            if (clientsign.Length > 0)
                            {
                                clientguid = Guid.NewGuid().ToString();
                                ByteArrayToImage(clientsign, ConfigurationManager.AppSettings["clientsignature"] + clientguid.Replace('/', ' '));
                                clientguid = clientguid + ".png";
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return serializer.Serialize(e.Message);
                    }




                    DbCommand cmmd = db.GetStoredProcCommand("addmaudittransaction_New");
                    DataTable dtmsg = new DataTable();
                    dtmsg.Columns.Add("Status");
                    dtmsg.Columns.Add("Message");
                    dtmsg.Columns.Add("guid");
                    dtmsg.Columns.Add("deviceid");

                    //objbasebo.UpdateLog("Before XML - Add");
                    //Addxml(XMLIs);
                    XMLIs = XMLIs.Replace("&", "&amp;");
                    db.AddInParameter(cmmd, "sbuid", DbType.Int32, sbuid);
                    db.AddInParameter(cmmd, "companyid", DbType.Int32, companyid);
                    db.AddInParameter(cmmd, "locationid", DbType.Int32, locationid);
                    db.AddInParameter(cmmd, "sectorid", DbType.Int32, sectorid);
                    db.AddInParameter(cmmd, "XMLIs", DbType.String, XMLIs);

                    //objbasebo.UpdateLog("After XML - Add");

                    db.AddInParameter(cmmd, "clientname", DbType.String, clientname);
                    db.AddInParameter(cmmd, "sitename", DbType.String, sitename);
                    db.AddInParameter(cmmd, "ssano", DbType.String, ssano);
                    db.AddInParameter(cmmd, "clientperson", DbType.String, clientperson);
                    db.AddInParameter(cmmd, "sbuname", DbType.String, sbuname);
                    db.AddInParameter(cmmd, "aomname", DbType.String, aomname);
                    db.AddInParameter(cmmd, "auditorname", DbType.String, auditorname);
                    db.AddInParameter(cmmd, "auditeename", DbType.String, auditeename);
                    db.AddInParameter(cmmd, "deviceID", DbType.String, deviceID);
                    db.AddInParameter(cmmd, "guiD", DbType.String, guiD);
                    db.AddInParameter(cmmd, "userid", DbType.Int32, userid);
                    db.AddInParameter(cmmd, "auddate", DbType.DateTime, auddate);
                    db.AddInParameter(cmmd, "auditorguid", DbType.String, auditorguid);
                    db.AddInParameter(cmmd, "clientguid", DbType.String, clientguid);
                    db.AddInParameter(cmmd, "observation", DbType.String, observation);
                    db.AddInParameter(cmmd, "feedback", DbType.String, feedback);

                    db.AddInParameter(cmmd, "CrticalOperations", DbType.String, Operations);
                    db.AddInParameter(cmmd, "CrticalTraining", DbType.String, Training);
                    db.AddInParameter(cmmd, "CrticalSCM", DbType.String, Scm);
                    db.AddInParameter(cmmd, "CrticalCompliance", DbType.String, Compliance);
                    db.AddInParameter(cmmd, "CrticalHR", DbType.String, hr);
                    db.AddInParameter(cmmd, "CrticalOthers", DbType.String, Others);

                    db.AddInParameter(cmmd, "FinalScore", DbType.String, totalScore);

                    db.AddInParameter(cmmd, "towername", DbType.String, towername);

                    string sfollowupdate = string.Empty;
                    if (followup.Trim() != string.Empty)
                    {
                        db.AddInParameter(cmmd, "FollowUpDate", DbType.DateTime, Convert.ToDateTime(followup));
                        sfollowupdate = Convert.ToDateTime(followup).ToShortDateString();
                    }
                    else
                    {
                        db.AddInParameter(cmmd, "FollowUpDate", DbType.DateTime, DBNull.Value);
                        sfollowupdate = string.Empty;
                    }

                    Decimal dTotalScore = Convert.ToDecimal(totalScore);
                    int iClosureDate = 0;
                    if (dTotalScore >= 83)
                    {
                        iClosureDate = 10;
                    }
                    else
                    {
                        iClosureDate = 7;
                    }
                    db.AddInParameter(cmmd, "AuditClosureDate", DbType.Int32, iClosureDate);

                    string[] Values = new string[3];
                    db.AddOutParameter(cmmd, "@ErrorMessage", DbType.String, 1000);
                    db.AddOutParameter(cmmd, "@Status", DbType.String, 1000);
                    db.ExecuteNonQuery(cmmd);
                    Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                    db.GetParameterValue(cmmd, "@ErrorMessage"));
                    Values[1] = db.GetParameterValue(cmmd, "@Status").ToString();

                    dtmsg.Rows.Add("1", "Savedsucessfully", guiD, deviceID);
                    if (Values[1] != "-1")
                    {
                        try
                        {
                            objbasebo.UpdateLog("Mail Function Start");
                            SendMail(auddate, companyid, locationid, sbuid, sectorid, Operations, Training, Scm, Compliance, hr, Others, sfollowupdate, iClosureDate, totalScore, sbuname, feedback, towername);
                            objbasebo.UpdateLog("Mail Function End");
                        }
                        catch (Exception ex)
                        {
                            objbasebo.UpdateLog("Error:" + ex.Message);
                        }
                    }
                    return ConvertJavaSeriptSerializer(dtmsg);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("PRIMARY KEY"))
                    {
                        DataTable dtmessage = new DataTable();
                        dtmessage.Columns.Add("deviceid");
                        dtmessage.Columns.Add("guid");
                        dtmessage.Columns.Add("Status");
                        dtmessage.Rows.Add(deviceID, guiD, 1);
                        return ConvertJavaSeriptSerializer(dtmessage);
                    }
                    objbasebo.UpdateLog("AddFeedBackDetails Error -" + auditdate + "-" + locationid.ToString() + "-" + e.Message.ToString());
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    return serializer.Serialize(e.Message);
                }
                #endregion
            }

        }

        private void Addxml(string xml)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmmd = db.GetStoredProcCommand("Addxml");
            db.AddInParameter(cmmd, "xml", DbType.String, xml);
            db.ExecuteNonQuery(cmmd);
        }

        #region "Split Function"

        private string GetSiteID(string sitename)
        {
            string[] sitedet = sitename.Split('-');
            return sitedet[sitedet.Length - 1];
        }

        #endregion

        #region "Send Mail For ClientWise  MailIDs"
        private void SendMail(DateTime auditdate, int companyid, int locationid, int sbuid, int sectorid, string operations, string training, string scm, string compliance, string hr, string others, string followupdate, int closuredate, string totalscore, string sbuname, string feedback, string towername)
        {
            try
            {
                DataSet dsRawdatas = GetRawDatasDateWiseReport(auditdate, locationid, sbuid, sectorid, towername);
                objbasebo.UpdateLog("Row Count : " + dsRawdatas.Tables[0].Rows.Count);
                if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                {
                    string path = System.AppDomain.CurrentDomain.BaseDirectory + "AuditFiles";
                    //string filename = path + "\\" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";


                    string filename = Guid.NewGuid().ToString();
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                        string file = drfilename["locsitename"].ToString().Replace("\t", string.Empty);
                        filename = file.Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty).Replace("*", string.Empty).Replace("?", string.Empty).Replace("\"", string.Empty);

                        RegexOptions options = RegexOptions.None;
                        Regex regex = new Regex("[ ]{2,}", options);
                        filename = regex.Replace(filename, " ");
                    }

                    if (dsRawdatas.Tables[4].Rows.Count > 0)
                    {
                        totalscore = dsRawdatas.Tables[4].Rows[0][0].ToString();
                    }
                    string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                    objbasebo.UpdateLog("Work Book Path :" + excelfilepath);
                    //WriteToFile(filename);

                    //GenerateExcel(dsRawdatas, totalscore, filename);

                    MakeExcel(dsRawdatas, totalscore, excelfilepath);

                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sbuname = drHeader["locsitename"].ToString();

                    string bodymail = File.ReadAllText(Server.MapPath("AppThemes/images/MailTemplate.txt"), Encoding.UTF8);
                    string subject = "Quality Audit - " + sbuname + " - " + auditdate.ToShortDateString();
                    if (towername.Trim().ToLower() != "na")
                    {
                        subject = "Quality Audit - " + sbuname + " - " + towername + " - " + auditdate.ToShortDateString();
                    }

                    if (dsRawdatas != null && dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[5].Rows.Count > 0)
                    {
                        DataRow drsubjectbody = dsRawdatas.Tables[5].Rows[0];
                        subject = drsubjectbody["mailsubject"].ToString();
                        bodymail = drsubjectbody["mailbody"].ToString();
                    }

                    subject = subject.Replace("#AuditDate#", auditdate.ToShortDateString());
                    subject = subject.Replace("#SbuName#", sbuname);
                    subject = subject.Replace("#towername#", towername);

                    bodymail = bodymail.Replace("#Operations#", operations);
                    bodymail = bodymail.Replace("#Training#", training);
                    bodymail = bodymail.Replace("#SCM#", scm);
                    bodymail = bodymail.Replace("#Compliance#", compliance);
                    bodymail = bodymail.Replace("#HR#", hr);
                    bodymail = bodymail.Replace("#Others#", others);
                    bodymail = bodymail.Replace("#Score#", totalscore + "%");
                    bodymail = bodymail.Replace("#Closure#", auditdate.AddDays(closuredate).ToShortDateString());
                    bodymail = bodymail.Replace("#SbuName#", sbuname);
                    bodymail = bodymail.Replace("#feedback#", feedback);

                    string tower = string.Empty;
                    if (towername.ToLower().Trim() != "na")
                    {
                        tower = "- " + towername + " ";
                    }

                    bodymail = bodymail.Replace("#towername#", tower);



                    if (followupdate.Trim() != string.Empty)
                    {
                        bodymail = bodymail.Replace("#Follow#", followupdate);
                    }
                    else
                    {
                        bodymail = bodymail.Replace("#Follow#", "Not Applicable");
                    }

                    SendingMail(bodymail, subject, excelfilepath, companyid, locationid, training, hr, scm, compliance, operations, auditdate, sbuid);
                    objbasebo.UpdateLog("Saved :" + path);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //private void SendObservationsMail(string operations, string training, string scm, string compliance, string hr, string others, int companyid, int locationid)
        //{
        //    try
        //    {
        //        DataSet dsEmailDet = getlocationmailid(companyid, locationid);
        //        if (dsEmailDet.Tables.Count > 0 && dsEmailDet.Tables[0].Rows.Count > 0)
        //        {
        //            DataRow drEmail = dsEmailDet.Tables[0].Rows[0];
        //            string operationheadname = drEmail["OperationHeadName"].ToString().Trim();
        //            string operationheadmailid = drEmail["OperationHeadMail"].ToString().Trim();

        //            string TrainingHeadName = drEmail["TrainingHeadName"].ToString().Trim();
        //            string TrainingHeadMail = drEmail["TrainingHeadMail"].ToString().Trim();

        //            string SCMHeadName = drEmail["SCMHeadName"].ToString().Trim();
        //            string SCMHeadMail = drEmail["SCMHeadMail"].ToString().Trim();

        //            string ComplianceHeadName = drEmail["ComplianceHeadName"].ToString().Trim();
        //            string ComplianceHeadMail = drEmail["ComplianceHeadMail"].ToString().Trim();

        //            string HRHeadName = drEmail["HRHeadName"].ToString().Trim();
        //            string HRHeadMail = drEmail["HRHeadMail"].ToString().Trim();


        //            string OthersHeadName = drEmail["OthersHeadName"].ToString().Trim();
        //            string OthersHeadMail = drEmail["OthersHeadMail"].ToString().Trim();

        //            string subject = string.Empty;
        //            string body = string.Empty;
        //            string ccmailids = string.Empty;
        //            if (operationheadmailid != string.Empty)
        //            {
        //                subject = "Critcal Observation - Operations";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, operationheadmailid, ccmailids);

        //            }
        //            if (TrainingHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - Training";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, TrainingHeadMail, ccmailids);
        //            }
        //            if (SCMHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - SCM";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, SCMHeadMail, ccmailids);
        //            }

        //            if (ComplianceHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - Compliance";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, ComplianceHeadMail, ccmailids);
        //            }

        //            if (HRHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - HR";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, HRHeadMail, ccmailids);
        //            }
        //            if (OthersHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - Others";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, OthersHeadMail, ccmailids);
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        private void MailObservations(string body, string subject, string toMail, string ccmail)
        {
            try
            {
                string username = string.Empty;
                string password = string.Empty;
                string smtpServer = string.Empty;
                int portno = 0;


                MailMessage mailMessage = new MailMessage();
                if (ConfigurationManager.AppSettings["UserName"] != null)
                {
                    username = ConfigurationManager.AppSettings["UserName"];
                }

                if (ConfigurationManager.AppSettings["Password"] != null)
                {
                    password = ConfigurationManager.AppSettings["Password"];
                }

                if (ConfigurationManager.AppSettings["SmtpServer"] != null)
                {
                    smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                }

                if (ConfigurationManager.AppSettings["Port"] != null)
                {
                    portno = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                }

                //usermail = "venkadesan.n@i2isoftwares.com";
                //ccmail="aravind@i2isoftwares.com";
                if (toMail != string.Empty)
                {
                    mailMessage.To.Add(toMail);
                    if (ccmail.Trim() != string.Empty)
                    {
                        foreach (var address in ccmail.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (address != string.Empty)
                            {
                                if (IsValidEmail(address.Trim()))
                                {
                                    mailMessage.CC.Add(address.Trim());
                                }
                            }
                        }
                    }

                    mailMessage.From = new MailAddress(username, "Facility Audit - Crtical Observations");
                    mailMessage.Subject = subject;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Body = body;
                    SmtpClient smtpClient = new SmtpClient(smtpServer, portno);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
                    smtpClient.EnableSsl = true;
                    smtpClient.Send(mailMessage);

                    objbasebo.UpdateLog("Mail Sent To :" + toMail + " CC : " + ccmail);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckNotApplicable(string crticalobservation)
        {
            try
            {
                bool bObservation = false;
                string[] stringArray = { "nil", "na", "n/a" };
                string value = crticalobservation.ToLower().Trim();
                int pos = Array.IndexOf(stringArray, value);
                if (pos > -1)
                {
                    bObservation = true;
                }
                return bObservation;
            }
            catch (Exception)
            {
                throw;
            }
        }


        private void SendingMail(string body, string subject, string attachment, int companyid, int locationid,
            string training, string hr, string scm, string compliance, string operations, DateTime Auditdate, int sbuid)
        {
            try
            {

                bool btraining = false, bhr = false, bscm = false, bcompliance = false, boperations = false;
                btraining = CheckNotApplicable(training);
                bhr = CheckNotApplicable(hr);
                bscm = CheckNotApplicable(scm);
                bcompliance = CheckNotApplicable(compliance);
                boperations = CheckNotApplicable(operations);
                InsertMailTrackingDetails(Auditdate, companyid, locationid, sbuid, subject, body, attachment); // Insert mail details
                DataSet dsEmailDet = getlocationmailid(companyid, locationid, btraining, bhr, bscm, bcompliance, boperations);
                if (dsEmailDet.Tables.Count > 0 && dsEmailDet.Tables[0].Rows.Count > 0)
                {
                    DataRow drEmail = dsEmailDet.Tables[0].Rows[0];
                    string toMail = drEmail["ToMail"].ToString();
                    string CcMail = drEmail["CCMail"].ToString();

                    string username = string.Empty;
                    string password = string.Empty;
                    string smtpServer = string.Empty;
                    string frommail = string.Empty;
                    int portno = 0;


                    MailMessage mailMessage = new MailMessage();
                    if (ConfigurationManager.AppSettings["UserName"] != null)
                    {
                        username = ConfigurationManager.AppSettings["UserName"];
                    }

                    if (ConfigurationManager.AppSettings["Password"] != null)
                    {
                        password = ConfigurationManager.AppSettings["Password"];
                    }

                    if (ConfigurationManager.AppSettings["SmtpServer"] != null)
                    {
                        smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                    }

                    if (ConfigurationManager.AppSettings["Port"] != null)
                    {
                        portno = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    }

                    if (ConfigurationManager.AppSettings["FromMail"] != null)
                    {
                        frommail = ConfigurationManager.AppSettings["FromMail"];
                    }

                    //usermail = "venkadesan.n@i2isoftwares.com";
                    //ccmail="aravind@i2isoftwares.com";
                    if (toMail.Trim() != string.Empty)
                    {
                        string newccmails = string.Empty;
                        foreach (var address in toMail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            mailMessage.To.Add(address);
                        }
                        if (CcMail.Trim() != string.Empty)
                        {
                            string[] emailaddress = CcMail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                            foreach (var address in emailaddress)
                            {
                                if (address != string.Empty)
                                {
                                    if (IsValidEmail(address.Trim()))
                                    {
                                        newccmails = newccmails + address + ",";
                                        mailMessage.CC.Add(address.Trim());
                                    }
                                }
                            }
                        }
                        newccmails = newccmails.TrimEnd(',');
                        UpdateMailIds(Auditdate, locationid, newccmails, toMail, false, string.Empty);

                        mailMessage.From = new MailAddress(frommail, "Facility Audit");
                        mailMessage.Subject = subject;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.Body = body;
                        mailMessage.Bcc.Add(frommail);
                        mailMessage.Attachments.Add(new Attachment(attachment));
                        SmtpClient smtpClient = new SmtpClient(smtpServer, portno);
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
                        smtpClient.EnableSsl = true;
                        smtpClient.Send(mailMessage);

                        //FileInfo prevFile = new FileInfo(attachment);
                        //if (prevFile.Exists)
                        //{
                        //    prevFile.Delete();
                        //}
                        UpdateMessage(Auditdate, locationid, "Mail Sent Sucessfully.", true);
                        objbasebo.UpdateLog("Mail Sent To :" + toMail + " CC : " + CcMail);
                    }
                    else
                    {
                        UpdateMailIds(Auditdate, locationid, CcMail, toMail, false, "No Email ID(s) Found.");
                    }
                }
            }
            catch (Exception ex)
            {
                UpdateMessage(Auditdate, locationid, ex.Message, false);
                throw;
            }
        }

        private void GenerateExcel(DataSet dsRawdatas, string Getscored, string filename)
        {
            try
            {
                StringBuilder sDatas = new StringBuilder();

                float fEnteirAvg = 0;
                int iTotalScore = 0;
                int iTotalWeightage = 0;
                int iMaxScore = 0;
                {
                    sDatas.Append("<table width='100%' cellspacing='0' cellpadding='2' border = '1'>");
                    DataTable dtAudit = dsRawdatas.Tables[0];
                    DataTable dtCategory = dsRawdatas.Tables[1];
                    DataTable dtRawData = dsRawdatas.Tables[2];
                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sDatas.Append("<tr><td rowspan='5' style='border: none;' ><img style='height: 54px;width: 100px;' border='0' alt='Dusters' src='http://ifazility.com/facilityaudit/AppThemes/images/JLL%20New.png'></td><td rowspan='4' style='font-size: 24px;border: none;font-weight:bold;' align='center' colspan='6'>DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD</td>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client</td><td colspan='3'>" + drHeader["clientname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Site</td><td colspan='3'>" + drHeader["locsitename"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Site ID</td><td colspan='3'>" + GetSiteID(drHeader["locsitename"].ToString()) + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client person</td><td colspan='3'>" + drHeader["clientperson"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the SBU</td><td colspan='3'>" + drHeader["sbuname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the OM / AOM</td><td colspan='3'>" + drHeader["aom"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditor</td><td colspan='3'>" + drHeader["auditor"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditee</td><td colspan='3'>" + drHeader["auditee"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Last audit date</td><td align='left' colspan='3'>" + drHeader["displastauditdate"].ToString() + "</td><td colspan='2'>Last Audit Score</td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Current audit date</td><td align='left' colspan='3'>" + drHeader["dispauditdate"].ToString() + "</td><td>Current Audit Score</td><td>" + Getscored + "%</td></tr>");

                    for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                    {
                        DataRow draudit = dtAudit.Rows[iauditcount];
                        int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                        string auditname = draudit["auditname"].ToString();
                        sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>" + auditname + "</td></tr>");
                        sDatas.Append("<tr><th style = 'background-color: #FCB133;color:#ffffff'>Sl.No</th><th style = 'background-color: #FCB133;color:#ffffff'>Observations</th><th style = 'background-color: #FCB133;color:#ffffff;width: 5%;'>Maximum <br/> Score</th><th style = 'background-color: #FCB133;color:#ffffff;width: 5%;'>Score <br/> Obtained</th><th style = 'background-color: #FCB133;color:#ffffff;width: 30%;'>Auditor Remarks</th><th style = 'background-color: #FCB133;color:#ffffff'>Remarks by SBU </th><th style = 'background-color: #FCB133;color:#ffffff'>Image</th></tr>");
                        int iscore = 0;
                        int iweightage = 0;
                        int iSingleAvg = 0;
                        int iTotalAvg = 0;
                        int iAuditMax = 0;
                        DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                        for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                        {
                            int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                            string categoryname = drCategory[icategroryCount]["categoryname"].ToString();
                            sDatas.Append("<tr><td align='center'>" + (icategroryCount + 1) + "</td><td style='font-size: 18px;' colspan='6'>" + categoryname + "</td></tr>");
                            DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                            int iCategoryScore = 0;
                            int iCategoryTotal = 0;
                            int iCatMax = 0;

                            for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                            {
                                string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                string filelink = string.Empty;
                                if (imgFile.Trim() != string.Empty)
                                {
                                    string[] imgfiles = imgFile.Split('#');
                                    foreach (string files in imgfiles)
                                    {
                                        if (files.Trim() != string.Empty)
                                        {
                                            imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                            filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";
                                        }
                                    }
                                }

                                int tScore = 0;
                                int MaxScore = 0;

                                if (score == -1)
                                {
                                    score = 0;
                                    tScore = -1;
                                    weightage = 0;
                                    MaxScore = 0;
                                }
                                else
                                {
                                    MaxScore = 1;
                                }

                                iscore += score;
                                iweightage += weightage;

                                iCategoryScore += score;
                                iCategoryTotal += weightage;

                                iTotalScore += score;
                                iTotalWeightage += weightage;

                                iSingleAvg = score * weightage;
                                iTotalAvg += iSingleAvg;

                                iMaxScore += MaxScore;
                                iCatMax += MaxScore;
                                iAuditMax += MaxScore;

                                string _score = string.Empty;
                                string _maxscore = string.Empty;
                                if (tScore == 0)
                                {
                                    _score = " " + score;
                                    _maxscore = " " + MaxScore;
                                }
                                else
                                {
                                    _score = scorename;
                                    _maxscore = scorename;
                                }
                                sDatas.Append("<tr><td align='center'>" + (iRawDatacount + 1).ToString() + "</td><td>" + question + "</td><td style='width: 5%;' align='center'>" + _maxscore + "</td><td  style='width: 5%;'  align='center'>" + _score + "</td><td>" + remarks + "</td><td style='width: 30%;'></td><td>" + filelink + "</td></tr>");
                            }
                            string sCatNA = string.Empty;
                            string sCatMaxNA = string.Empty;
                            sCatNA = iCatMax.ToString();
                            sCatMaxNA = iCategoryScore.ToString();
                            if (iCatMax == 0)
                            {
                                sCatNA = "N/A";
                            }
                            if (iCategoryScore == 0)
                            {
                                sCatMaxNA = "N/A";
                            }
                            sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>" + categoryname + "</td><td align='center'>" + sCatNA + "</td><td align='center'>" + sCatMaxNA + "</td><td></td><td></td><td></td></tr>");
                        }
                        float iScoredAvg = 0;
                        iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                        fEnteirAvg += iScoredAvg;

                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Total Score for " + auditname + "</td><td align='center'>" + iAuditMax + "</td><td align='center'>" + iscore + "</td><td></td><td></td><td></td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td><td align='center'>" + iScoredAvg + "%" + "</td><td></td><td></td><td></td></tr>");
                    }
                    float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>GRAND TOTAL SCORE</td><td align='center'>" + iMaxScore + "</td><td align='center'>" + iTotalScore + "</td><td></td><td></td><td></td></tr>");

                    decimal scoreGot = Convert.ToDecimal(Getscored);
                    string setColour = string.Empty;
                    if (scoreGot >= 83)
                    {
                        setColour = "<td style='background-color: #00FF00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    else
                    {
                        setColour = "<td style='background-color: #FFBE00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td>" + setColour + "<td></td><td></td><td></td></tr>");
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Follow-Up Date</td><td></td><td>" + dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString() + "</td><td></td><td></td><td></td></tr>");
                    }


                    string observations = drHeader["observation"].ToString();
                    string clientremarks = drHeader["feedback"].ToString();
                    observations = observations.Replace('[', ' ');
                    observations = observations.Replace(']', ' ');
                    try
                    {
                        Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                        //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                        //"2. Training  : " + objobservation.JS_Training + " break  " +
                        //"3. SCM  : " + objobservation.JS_SCM + " break " +
                        //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                        //"5. HR  : " + objobservation.JS_HR + "break " +
                        //"6. Others : " + objobservation.JS_Operation;
                        if (objobservation != null)
                        {
                            sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Other Observations</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Operations</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Training</td><td colspan='5'>" + objobservation.JS_Training + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>SCM</td><td colspan='5'>" + objobservation.JS_SCM + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Compliance</td><td colspan='5'>" + objobservation.JS_Complaince + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>HR</td><td colspan='5'>" + objobservation.JS_HR + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Others</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                        }
                    }
                    catch (Exception) { }
                    sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Client Feedback</td></tr>");
                    sDatas.Append("<tr><td  colspan='7'>" + clientremarks + "</td></tr>");

                    sDatas.Append("</table>");



                    string memString = sDatas.ToString();
                    // convert string to stream
                    byte[] buffer = Encoding.ASCII.GetBytes(memString);
                    MemoryStream ms = new MemoryStream(buffer);
                    //write to file
                    FileStream file = new FileStream(filename, FileMode.Create, FileAccess.Write);
                    ms.WriteTo(file);
                    file.Close();
                    ms.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void MakeExcel(DataSet dsRawdatas, string Getscored, string excelfilepath)
        {
            try
            {
                //string excelfilepath = Server.MapPath("DownloadExcels/") + Guid.NewGuid().ToString() + ".xlsx";
                FileInfo newFile = new FileInfo(excelfilepath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(excelfilepath);
                }

                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

                    //Add the headers

                    StringBuilder sDatas = new StringBuilder();

                    float fEnteirAvg = 0;
                    int iTotalScore = 0;
                    int iTotalWeightage = 0;
                    int iMaxScore = 0;

                    DataTable dtChartdet = new DataTable();
                    dtChartdet.Columns.Add("Categoryname", typeof(string));
                    dtChartdet.Columns.Add("Value", typeof(float));
                    dtChartdet.Columns.Add("auditId", typeof(int));

                    if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtAudit = dsRawdatas.Tables[0];
                        DataTable dtCategory = dsRawdatas.Tables[1];
                        DataTable dtRawData = dsRawdatas.Tables[2];
                        DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        // worksheet.Cells[1, 1].Value = "Image Here";
                        //worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

                        worksheet.Cells[1, 1].Value = drHeader["GroupName"].ToString().ToUpper();

                        //var headerFont = worksheet.Cells[1, 1].Style.Font;
                        //headerFont.Bold = true;
                        //headerFont.Size = 18;
                        //worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        //worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                        worksheet.Cells["A1:G4"].Merge = true;

                        //FileInfo fileinfo = new FileInfo(Server.MapPath("AppThemes/images/dts.png"));
                        //var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                        //picture.SetSize(100, 50);
                        //picture.SetPosition(1, 0, 1, 0);

                        string logofile = drHeader["UploadLogoName"].ToString();
                        if (logofile.Trim() != string.Empty)
                        {
                            string filepath = ConfigurationManager.AppSettings["companylogo"].ToString() + logofile;

                            FileInfo fileinfo = new FileInfo(filepath);
                            var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                            picture.SetSize(100, 50);
                            picture.SetPosition(1, 0, 1, 0);
                        }

                        worksheet.Cells[5, 1].Value = "Name of the Client";

                        worksheet.Cells[5, 3].Value = drHeader["clientname"].ToString();
                        worksheet.Cells[5, 6].Value = "";



                        worksheet.Cells["A5:B5"].Merge = true;
                        worksheet.Cells["C5:E5"].Merge = true;
                        worksheet.Cells["F5:G5"].Merge = true;

                        int imaxrowcount = 6;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Site";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["locsitename"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Name = drHeader["locsitename"].ToString();

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Tower";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["towername"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Site ID";
                        worksheet.Cells[imaxrowcount, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Client person";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["clientperson"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the SBU";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["sbuname"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the OM / AOM";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["aom"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Auditor";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["auditor"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Auditee";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["auditee"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = "Last audit date";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["displastauditdate"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "Last Audit Score";
                        worksheet.Cells[imaxrowcount, 7].Value = drHeader["lastscore"].ToString() + "%";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        //worksheet.Cells["F13:G13"].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Current audit date";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["dispauditdate"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "Current Audit Score";
                        worksheet.Cells[imaxrowcount, 7].Value = Getscored + "%";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;


                        for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        {
                            DataRow draudit = dtAudit.Rows[iauditcount];
                            int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                            string auditname = draudit["auditname"].ToString();

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                            worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
                            worksheet.Cells[imaxrowcount, 2].Value = "Observations";
                            worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
                            worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
                            worksheet.Cells[imaxrowcount, 5].Value = "Auditor Remarks";
                            worksheet.Cells[imaxrowcount, 6].Value = "Remarks by Sbu";
                            worksheet.Cells[imaxrowcount, 7].Value = "Image";

                            var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 7];
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                            imaxrowcount++;

                            int iscore = 0;
                            int iweightage = 0;
                            int iSingleAvg = 0;
                            int iTotalAvg = 0;
                            int iAuditMax = 0;
                            DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                            for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                            {
                                int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                                string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                                worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
                                worksheet.Cells[imaxrowcount, 2].Value = categoryname;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

                                worksheet.Cells["B" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                                int iCategoryScore = 0;
                                int iCategoryTotal = 0;
                                int iCatMax = 0;

                                int iCatWisePercentage = 0;
                                int iCatWiseTotal = 0;

                                for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                                {
                                    string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                    string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                    int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                    int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                    string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                    string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                    string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
                                    string filelink = string.Empty;
                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
                                        if (ConfigurationManager.AppSettings["Imaglists"] != null)
                                        {
                                            filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
                                        }
                                        filelink = filepath + "?TransactionId=" + transactionid;
                                        //string[] imgfiles = imgFile.Split('#');
                                        //foreach (string files in imgfiles)
                                        //{
                                        //    if (files.Trim() != string.Empty)
                                        //    {
                                        //        imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                        //        //filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";

                                        //        filelink = "HYPERLINK(\"" + imgFile + "\",\"" + files + "\")";
                                        //    }
                                        //}
                                    }

                                    int tScore = 0;
                                    int MaxScore = 0;

                                    if (score == -1)
                                    {
                                        score = 0;
                                        tScore = -1;
                                        weightage = 0;
                                        MaxScore = 0;
                                    }
                                    else
                                    {
                                        MaxScore = 1;
                                    }

                                    iscore += score;
                                    iweightage += weightage;

                                    iCategoryScore += score;
                                    iCategoryTotal += weightage;

                                    iTotalScore += score;
                                    iTotalWeightage += weightage;

                                    iSingleAvg = score * weightage;
                                    iTotalAvg += iSingleAvg;

                                    iCatWisePercentage = score * weightage;
                                    iCatWiseTotal += iCatWisePercentage;

                                    iMaxScore += MaxScore;
                                    iCatMax += MaxScore;
                                    iAuditMax += MaxScore;

                                    string _score = string.Empty;
                                    string _maxscore = string.Empty;
                                    if (tScore == 0)
                                    {
                                        _score = " " + score;
                                        _maxscore = " " + MaxScore;
                                    }
                                    else
                                    {
                                        _score = scorename;
                                        _maxscore = scorename;
                                    }


                                    worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
                                    worksheet.Cells[imaxrowcount, 2].Value = question;

                                    if (question.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    }

                                    //worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

                                    int MaxRefId;
                                    int ScorerefID;

                                    bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = _maxscore;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = _score;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    //worksheet.Cells[imaxrowcount, 4].Value = _score;
                                    worksheet.Cells[imaxrowcount, 5].Value = remarks;

                                    if (remarks.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 5].Style.WrapText = true;
                                    }

                                    worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        var cell = worksheet.Cells[imaxrowcount, 7];
                                        cell.Hyperlink = new Uri(filelink);
                                        cell.Value = "ImageList";
                                    }

                                    // worksheet.Cells[imaxrowcount, 7].Formula = filelink;
                                    //worksheet.Cells[imaxrowcount, 7].Style.WrapText = true;
                                    imaxrowcount++;
                                }

                                float _CateWisTot = 0;
                                if (iCategoryTotal != 0)
                                {
                                    //_CateWisTot = ((float)(iCategoryScore * 100 / iCategoryTotal));
                                    _CateWisTot = ((float)(iCatWiseTotal * 100 / iCategoryTotal));
                                }
                                else
                                {
                                    _CateWisTot = 0;
                                }

                                dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


                                string sCatNA = string.Empty;
                                string sCatMaxNA = string.Empty;
                                sCatNA = iCatMax.ToString();
                                sCatMaxNA = iCategoryScore.ToString();
                                if (iCatMax == 0)
                                {
                                    sCatNA = "N/A";
                                }
                                if (iCategoryScore == 0)
                                {
                                    sCatMaxNA = "N/A";
                                }

                                worksheet.Cells[imaxrowcount, 1].Value = "Sub Total " + categoryname;

                                int _catena;
                                int _catmaxna;
                                bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = _catena;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }

                                worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                            float iScoredAvg = 0;
                            if (iTotalAvg > 0)
                            {
                                iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                            }
                            fEnteirAvg += iScoredAvg;

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 3].Value = iAuditMax;
                            worksheet.Cells[imaxrowcount, 4].Value = iscore;

                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                            worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;
                        }
                        //float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                        worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
                        worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
                        worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;

                        decimal scoreGot = Convert.ToDecimal(Getscored);


                        worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = Getscored + "%";
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        string setColour = string.Empty;
                        if (scoreGot >= 83)
                        {
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
                        }
                        else
                        {
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
                        }
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;



                        if (dsRawdatas.Tables[3].Rows.Count > 0)
                        {
                            worksheet.Cells[imaxrowcount, 1].Value = "Follow-Up Date";
                            worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            worksheet.Cells[imaxrowcount, 4].Value = dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString();
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;
                        }


                        string observations = drHeader["observation"].ToString();
                        string clientremarks = drHeader["feedback"].ToString();
                        observations = observations.Replace('[', ' ');
                        observations = observations.Replace(']', ' ');
                        try
                        {
                            Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                            //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                            //"2. Training  : " + objobservation.JS_Training + " break  " +
                            //"3. SCM  : " + objobservation.JS_SCM + " break " +
                            //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                            //"5. HR  : " + objobservation.JS_HR + "break " +
                            //"6. Others : " + objobservation.JS_Operation;
                            if (objobservation != null)
                            {

                                worksheet.Cells[imaxrowcount, 1].Value = "Other Observations";
                                worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Operations";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Operation;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Training";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Training;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "SCM";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_SCM;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Compliance";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Complaince;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "HR";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_HR;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Others";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Others;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                        }
                        catch (Exception) { }

                        worksheet.Cells[imaxrowcount, 1].Value = "Client Feedback";
                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        // worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
                        // worksheet.Cells.AutoFitColumns(1);
                        //// worksheet.Cells.AutoFitColumns(2);
                        worksheet.Column(2).Width = 140;
                        worksheet.Column(5).Width = 140;
                        worksheet.Cells.AutoFitColumns(1);
                        worksheet.Cells.AutoFitColumns(2);
                        worksheet.Cells.AutoFitColumns(3);
                        // worksheet.Cells.AutoFitColumns(4);
                        //worksheet.Cells.AutoFitColumns(5);
                        // worksheet.Cells.AutoFitColumns(6);
                        // worksheet.Cells.AutoFitColumns(7);

                        int c1;

                        imaxrowcount = 7;
                        ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int startchar = 0;
                        int colspace = 0;
                        foreach (DataRow drAuditRow in dtAudit.Rows)
                        {
                            int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
                            string auditname = drAuditRow["auditname"].ToString();
                            DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
                            if (drAuditCategories.Length > 0)
                            {
                                DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
                                int startvalue = imaxrowcount;

                                char Firstcol = Alphabetic[startchar];
                                char Secondcol = Alphabetic[startchar + 1];

                                foreach (DataRow drchartdet in dtCategoryChart.Rows)
                                {
                                    string categoryname = drchartdet["categoryname"].ToString();
                                    decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

                                    worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
                                    worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
                                    imaxrowcount++;
                                }

                                var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
                                var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
                                //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
                                chart.SetSize(510, 300);
                                chart.Title.Text = auditname;
                                chart.SetPosition(7, 0, colspace, 0);
                                colspace += 9;

                                startchar += 2;
                            }
                        }


                        package.Save();

                        //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        //this.Response.AddHeader(
                        //          "content-disposition",
                        //          string.Format("attachment;  filename={0}", "ExcellData.xlsx"));
                        //this.Response.BinaryWrite(package.GetAsByteArray());
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //private void MakeExcel(DataSet dsRawdatas, string Getscored, string excelfilepath)
        //{
        //    try
        //    {
        //        //string excelfilepath = Server.MapPath("DownloadExcels/") + Guid.NewGuid().ToString() + ".xlsx";
        //        FileInfo newFile = new FileInfo(excelfilepath);
        //        if (newFile.Exists)
        //        {
        //            newFile.Delete();  // ensures we create a new workbook
        //            newFile = new FileInfo(excelfilepath);
        //        }

        //        using (ExcelPackage package = new ExcelPackage(newFile))
        //        {
        //            // add a new worksheet to the empty workbook
        //            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

        //            //Add the headers

        //            StringBuilder sDatas = new StringBuilder();

        //            float fEnteirAvg = 0;
        //            int iTotalScore = 0;
        //            int iTotalWeightage = 0;
        //            int iMaxScore = 0;

        //            DataTable dtChartdet = new DataTable();
        //            dtChartdet.Columns.Add("Categoryname", typeof(string));
        //            dtChartdet.Columns.Add("Value", typeof(float));
        //            dtChartdet.Columns.Add("auditId", typeof(int));

        //            if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
        //            {

        //                DataTable dtAudit = dsRawdatas.Tables[0];
        //                DataTable dtCategory = dsRawdatas.Tables[1];
        //                DataTable dtRawData = dsRawdatas.Tables[2];
        //                DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

        //                // worksheet.Cells[1, 1].Value = "Image Here";
        //                worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

        //                var headerFont = worksheet.Cells[1, 1].Style.Font;
        //                headerFont.Bold = true;
        //                headerFont.Size = 18;
        //                worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

        //                worksheet.Cells["A1:G4"].Merge = true;

        //                FileInfo fileinfo = new FileInfo(Server.MapPath("AppThemes/images/dts.png"));
        //                var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
        //                picture.SetSize(100, 50);
        //                picture.SetPosition(1, 0, 1, 0);

        //                worksheet.Cells[5, 1].Value = "Name of the Client";

        //                worksheet.Cells[5, 3].Value = drHeader["clientname"].ToString();
        //                worksheet.Cells[5, 6].Value = "";



        //                worksheet.Cells["A5:B5"].Merge = true;
        //                worksheet.Cells["C5:E5"].Merge = true;
        //                worksheet.Cells["F5:G5"].Merge = true;

        //                worksheet.Cells[6, 1].Value = "Name of the Site";
        //                worksheet.Cells[6, 3].Value = drHeader["locsitename"].ToString();
        //                worksheet.Cells[6, 6].Value = "";

        //                worksheet.Name = drHeader["locsitename"].ToString();

        //                worksheet.Cells["A6:B6"].Merge = true;
        //                worksheet.Cells["C6:E6"].Merge = true;
        //                worksheet.Cells["F6:G6"].Merge = true;

        //                worksheet.Cells[7, 1].Value = "Site ID";
        //                worksheet.Cells[7, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
        //                worksheet.Cells[7, 6].Value = "";

        //                worksheet.Cells["A7:B7"].Merge = true;
        //                worksheet.Cells["C7:E7"].Merge = true;
        //                worksheet.Cells["F7:G7"].Merge = true;

        //                worksheet.Cells[8, 1].Value = "Name of the Client person";
        //                worksheet.Cells[8, 3].Value = drHeader["clientperson"].ToString();
        //                worksheet.Cells[8, 6].Value = "";

        //                worksheet.Cells["A8:B8"].Merge = true;
        //                worksheet.Cells["C8:E8"].Merge = true;
        //                worksheet.Cells["F8:G8"].Merge = true;

        //                worksheet.Cells[9, 1].Value = "Name of the SBU";
        //                worksheet.Cells[9, 3].Value = drHeader["sbuname"].ToString();
        //                worksheet.Cells[9, 6].Value = "";

        //                worksheet.Cells["A9:B9"].Merge = true;
        //                worksheet.Cells["C9:E9"].Merge = true;
        //                worksheet.Cells["F9:G9"].Merge = true;

        //                worksheet.Cells[10, 1].Value = "Name of the OM / AOM";
        //                worksheet.Cells[10, 3].Value = drHeader["aom"].ToString();
        //                worksheet.Cells[10, 6].Value = "";

        //                worksheet.Cells["A10:B10"].Merge = true;
        //                worksheet.Cells["C10:E10"].Merge = true;
        //                worksheet.Cells["F10:G10"].Merge = true;

        //                worksheet.Cells[11, 1].Value = "Name of the Auditor";
        //                worksheet.Cells[11, 3].Value = drHeader["auditor"].ToString();
        //                worksheet.Cells[11, 6].Value = "";

        //                worksheet.Cells["A11:B11"].Merge = true;
        //                worksheet.Cells["C11:E11"].Merge = true;
        //                worksheet.Cells["F11:G11"].Merge = true;

        //                worksheet.Cells[12, 1].Value = "Name of the Auditee";
        //                worksheet.Cells[12, 3].Value = drHeader["auditee"].ToString();
        //                worksheet.Cells[12, 6].Value = "";

        //                worksheet.Cells["A12:B12"].Merge = true;
        //                worksheet.Cells["C12:E12"].Merge = true;
        //                worksheet.Cells["F12:G12"].Merge = true;

        //                worksheet.Cells[13, 1].Value = "Last audit date";
        //                worksheet.Cells[13, 3].Value = drHeader["displastauditdate"].ToString();
        //                worksheet.Cells[13, 6].Value = "Last Audit Score";
        //                worksheet.Cells[13, 7].Value = drHeader["lastscore"].ToString() + "%";

        //                worksheet.Cells["A13:B13"].Merge = true;
        //                worksheet.Cells["C13:E13"].Merge = true;
        //                //worksheet.Cells["F13:G13"].Merge = true;

        //                worksheet.Cells[14, 1].Value = "Current audit date";
        //                worksheet.Cells[14, 3].Value = drHeader["dispauditdate"].ToString();
        //                worksheet.Cells[14, 6].Value = "Current Audit Score";
        //                worksheet.Cells[14, 7].Value = Getscored + "%";

        //                worksheet.Cells["A14:B14"].Merge = true;
        //                worksheet.Cells["C14:E14"].Merge = true;

        //                int imaxrowcount = 15;

        //                for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
        //                {
        //                    DataRow draudit = dtAudit.Rows[iauditcount];
        //                    int auditid = Convert.ToInt32(draudit["auditid"].ToString());
        //                    string auditname = draudit["auditname"].ToString();

        //                    worksheet.Cells[imaxrowcount, 1].Value = auditname;
        //                    worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

        //                    worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                    worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                    imaxrowcount++;

        //                    worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
        //                    worksheet.Cells[imaxrowcount, 2].Value = "Observations";
        //                    worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
        //                    worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
        //                    worksheet.Cells[imaxrowcount, 5].Value = "Auditor Remarks";
        //                    worksheet.Cells[imaxrowcount, 6].Value = "Remarks by Sbu";
        //                    worksheet.Cells[imaxrowcount, 7].Value = "Image";

        //                    var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 7];
        //                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                    range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

        //                    imaxrowcount++;

        //                    int iscore = 0;
        //                    int iweightage = 0;
        //                    int iSingleAvg = 0;
        //                    int iTotalAvg = 0;
        //                    int iAuditMax = 0;
        //                    DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
        //                    for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
        //                    {
        //                        int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
        //                        string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

        //                        worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
        //                        worksheet.Cells[imaxrowcount, 2].Value = categoryname;
        //                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
        //                        worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
        //                        worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

        //                        worksheet.Cells["B" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                        imaxrowcount++;

        //                        DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

        //                        int iCategoryScore = 0;
        //                        int iCategoryTotal = 0;
        //                        int iCatMax = 0;

        //                        int iCatWisePercentage = 0;
        //                        int iCatWiseTotal = 0;

        //                        for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
        //                        {
        //                            string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
        //                            string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
        //                            int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
        //                            int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
        //                            string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
        //                            string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
        //                            string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
        //                            string filelink = string.Empty;
        //                            if (imgFile.Trim() != string.Empty)
        //                            {
        //                                string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
        //                                if (ConfigurationManager.AppSettings["Imaglists"] != null)
        //                                {
        //                                    filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
        //                                }
        //                                filelink = filepath + "?TransactionId=" + transactionid;
        //                                //string[] imgfiles = imgFile.Split('#');
        //                                //foreach (string files in imgfiles)
        //                                //{
        //                                //    if (files.Trim() != string.Empty)
        //                                //    {
        //                                //        imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
        //                                //        //filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";

        //                                //        filelink = "HYPERLINK(\"" + imgFile + "\",\"" + files + "\")";
        //                                //    }
        //                                //}
        //                            }

        //                            int tScore = 0;
        //                            int MaxScore = 0;

        //                            if (score == -1)
        //                            {
        //                                score = 0;
        //                                tScore = -1;
        //                                weightage = 0;
        //                                MaxScore = 0;
        //                            }
        //                            else
        //                            {
        //                                MaxScore = 1;
        //                            }

        //                            iscore += score;
        //                            iweightage += weightage;

        //                            iCategoryScore += score;
        //                            iCategoryTotal += weightage;

        //                            iTotalScore += score;
        //                            iTotalWeightage += weightage;

        //                            iSingleAvg = score * weightage;
        //                            iTotalAvg += iSingleAvg;

        //                            iCatWisePercentage = score * weightage;
        //                            iCatWiseTotal += iCatWisePercentage;

        //                            iMaxScore += MaxScore;
        //                            iCatMax += MaxScore;
        //                            iAuditMax += MaxScore;

        //                            string _score = string.Empty;
        //                            string _maxscore = string.Empty;
        //                            if (tScore == 0)
        //                            {
        //                                _score = " " + score;
        //                                _maxscore = " " + MaxScore;
        //                            }
        //                            else
        //                            {
        //                                _score = scorename;
        //                                _maxscore = scorename;
        //                            }


        //                            worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
        //                            worksheet.Cells[imaxrowcount, 2].Value = question;

        //                            if (question.Length >= 75)
        //                            {
        //                                worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
        //                            }

        //                            //worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
        //                            //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

        //                            int MaxRefId;
        //                            int ScorerefID;

        //                            bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

        //                            if (isNumeric)
        //                            {
        //                                worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
        //                                worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            }
        //                            else
        //                            {
        //                                worksheet.Cells[imaxrowcount, 3].Value = _maxscore;
        //                                worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            }

        //                            isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

        //                            if (isNumeric)
        //                            {
        //                                worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
        //                                worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            }
        //                            else
        //                            {
        //                                worksheet.Cells[imaxrowcount, 4].Value = _score;
        //                                worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            }

        //                            //worksheet.Cells[imaxrowcount, 4].Value = _score;
        //                            worksheet.Cells[imaxrowcount, 5].Value = remarks;

        //                            if (remarks.Length >= 75)
        //                            {
        //                                worksheet.Cells[imaxrowcount, 5].Style.WrapText = true;
        //                            }

        //                            worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

        //                            if (imgFile.Trim() != string.Empty)
        //                            {
        //                                var cell = worksheet.Cells[imaxrowcount, 7];
        //                                cell.Hyperlink = new Uri(filelink);
        //                                cell.Value = "ImageList";
        //                            }

        //                            // worksheet.Cells[imaxrowcount, 7].Formula = filelink;
        //                            //worksheet.Cells[imaxrowcount, 7].Style.WrapText = true;
        //                            imaxrowcount++;
        //                        }

        //                        float _CateWisTot = 0;
        //                        if (iCategoryTotal != 0)
        //                        {
        //                            //_CateWisTot = ((float)(iCategoryScore * 100 / iCategoryTotal));
        //                            _CateWisTot = ((float)(iCatWiseTotal * 100 / iCategoryTotal));
        //                        }
        //                        else
        //                        {
        //                            _CateWisTot = 0;
        //                        }

        //                        dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


        //                        string sCatNA = string.Empty;
        //                        string sCatMaxNA = string.Empty;
        //                        sCatNA = iCatMax.ToString();
        //                        sCatMaxNA = iCategoryScore.ToString();
        //                        if (iCatMax == 0)
        //                        {
        //                            sCatNA = "N/A";
        //                        }
        //                        if (iCategoryScore == 0)
        //                        {
        //                            sCatMaxNA = "N/A";
        //                        }

        //                        worksheet.Cells[imaxrowcount, 1].Value = "Sub Total " + categoryname;

        //                        int _catena;
        //                        int _catmaxna;
        //                        bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

        //                        if (Numeric)
        //                        {
        //                            worksheet.Cells[imaxrowcount, 3].Value = _catena;
        //                            worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        }
        //                        else
        //                        {
        //                            worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
        //                            worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        }
        //                        worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

        //                        Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

        //                        if (Numeric)
        //                        {
        //                            worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
        //                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        }
        //                        else
        //                        {
        //                            worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;
        //                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        }

        //                        worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                        worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

        //                        //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
        //                        //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

        //                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
        //                        worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
        //                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
        //                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                        imaxrowcount++;
        //                    }
        //                    float iScoredAvg = 0;
        //                    iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
        //                    fEnteirAvg += iScoredAvg;

        //                    worksheet.Cells[imaxrowcount, 1].Value = auditname;
        //                    worksheet.Cells[imaxrowcount, 3].Value = iAuditMax;
        //                    worksheet.Cells[imaxrowcount, 4].Value = iscore;

        //                    worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
        //                    worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
        //                    worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
        //                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        //                    worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                    imaxrowcount++;

        //                    worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
        //                    worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
        //                    worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

        //                    worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
        //                    worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
        //                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                    worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                    imaxrowcount++;
        //                }
        //                float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

        //                worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
        //                worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
        //                worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
        //                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
        //                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
        //                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
        //                worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
        //                worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
        //                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                imaxrowcount++;

        //                decimal scoreGot = Convert.ToDecimal(Getscored);


        //                worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
        //                worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
        //                worksheet.Cells[imaxrowcount, 4].Value = Getscored + "%";
        //                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
        //                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
        //                string setColour = string.Empty;
        //                if (scoreGot >= 83)
        //                {
        //                    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
        //                }
        //                else
        //                {
        //                    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
        //                }
        //                worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                imaxrowcount++;



        //                if (dsRawdatas.Tables[3].Rows.Count > 0)
        //                {
        //                    worksheet.Cells[imaxrowcount, 1].Value = "Follow-Up Date";
        //                    worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
        //                    worksheet.Cells[imaxrowcount, 4].Value = dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString();
        //                    worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

        //                    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
        //                    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
        //                    worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                    imaxrowcount++;
        //                }


        //                string observations = drHeader["observation"].ToString();
        //                string clientremarks = drHeader["feedback"].ToString();
        //                observations = observations.Replace('[', ' ');
        //                observations = observations.Replace(']', ' ');
        //                try
        //                {
        //                    Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
        //                    //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
        //                    //"2. Training  : " + objobservation.JS_Training + " break  " +
        //                    //"3. SCM  : " + objobservation.JS_SCM + " break " +
        //                    //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
        //                    //"5. HR  : " + objobservation.JS_HR + "break " +
        //                    //"6. Others : " + objobservation.JS_Operation;
        //                    if (objobservation != null)
        //                    {

        //                        worksheet.Cells[imaxrowcount, 1].Value = "Other Observations";
        //                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
        //                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
        //                        imaxrowcount++;

        //                        worksheet.Cells[imaxrowcount, 1].Value = "Operations";
        //                        worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Operation;
        //                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                        worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                        imaxrowcount++;

        //                        worksheet.Cells[imaxrowcount, 1].Value = "Training";
        //                        worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Training;
        //                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                        worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                        imaxrowcount++;

        //                        worksheet.Cells[imaxrowcount, 1].Value = "SCM";
        //                        worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_SCM;
        //                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                        worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                        imaxrowcount++;

        //                        worksheet.Cells[imaxrowcount, 1].Value = "Compliance";
        //                        worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Complaince;
        //                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                        worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                        imaxrowcount++;

        //                        worksheet.Cells[imaxrowcount, 1].Value = "HR";
        //                        worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_HR;
        //                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                        worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                        imaxrowcount++;

        //                        worksheet.Cells[imaxrowcount, 1].Value = "Others";
        //                        worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Others;
        //                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
        //                        worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                        imaxrowcount++;
        //                    }
        //                }
        //                catch (Exception) { }

        //                worksheet.Cells[imaxrowcount, 1].Value = "Client Feedback";
        //                worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
        //                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
        //                worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                imaxrowcount++;


        //                worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
        //                worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
        //                imaxrowcount++;


        //                worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //                worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //                worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //                worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

        //                // worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
        //                // worksheet.Cells.AutoFitColumns(1);
        //                //// worksheet.Cells.AutoFitColumns(2);
        //                worksheet.Column(2).Width = 140;
        //                worksheet.Column(5).Width = 140;
        //                worksheet.Cells.AutoFitColumns(1);
        //                worksheet.Cells.AutoFitColumns(2);
        //                worksheet.Cells.AutoFitColumns(3);
        //                // worksheet.Cells.AutoFitColumns(4);
        //                //worksheet.Cells.AutoFitColumns(5);
        //                // worksheet.Cells.AutoFitColumns(6);
        //                // worksheet.Cells.AutoFitColumns(7);

        //                int c1;

        //                imaxrowcount = 7;
        //                ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

        //                string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //                int startchar = 0;
        //                int colspace = 0;
        //                foreach (DataRow drAuditRow in dtAudit.Rows)
        //                {
        //                    int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
        //                    string auditname = drAuditRow["auditname"].ToString();
        //                    DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
        //                    if (drAuditCategories.Length > 0)
        //                    {
        //                        DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
        //                        int startvalue = imaxrowcount;

        //                        char Firstcol = Alphabetic[startchar];
        //                        char Secondcol = Alphabetic[startchar + 1];

        //                        foreach (DataRow drchartdet in dtCategoryChart.Rows)
        //                        {
        //                            string categoryname = drchartdet["categoryname"].ToString();
        //                            decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

        //                            worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
        //                            worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
        //                            imaxrowcount++;
        //                        }

        //                        var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
        //                        var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
        //                        //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
        //                        chart.SetSize(510, 300);
        //                        chart.Title.Text = auditname;
        //                        chart.SetPosition(7, 0, colspace, 0);
        //                        colspace += 9;

        //                        startchar += 2;
        //                    }
        //                }


        //                package.Save();

        //                //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //                //this.Response.AddHeader(
        //                //          "content-disposition",
        //                //          string.Format("attachment;  filename={0}", "ExcellData.xlsx"));
        //                //this.Response.BinaryWrite(package.GetAsByteArray());
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        #endregion

        #region "Get Mail Datatable"

        public DataSet GetRawDatasDateWiseReport(DateTime auditdate, int locationid, int sbuid, int sectorid, string towername)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetRawDatasDateWiseReport");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
                db.AddInParameter(cmd, "SectorId", DbType.Int32, sectorid);
                db.AddInParameter(cmd, "towername", DbType.String, towername);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch
            {
                throw;
            }
        }

        public void InsertMailTrackingDetails(DateTime auditdate, int companyid, int locationid, int sbuid, string subject, string body, string attachments)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_InsertMailingSystem");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
                db.AddInParameter(cmd, "Subject", DbType.String, subject);
                db.AddInParameter(cmd, "Body", DbType.String, body);
                db.AddInParameter(cmd, "Attachments", DbType.String, attachments);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateMailIds(DateTime auditdate, int locationid, string ccmail, string tomail, bool isSent, string message)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_UpdateMailIDs");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "ccmail", DbType.String, ccmail);
                db.AddInParameter(cmd, "ToMail", DbType.String, tomail);
                db.AddInParameter(cmd, "isSent", DbType.Boolean, isSent);
                db.AddInParameter(cmd, "Message", DbType.String, message);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateMessage(DateTime auditdate, int locationid, string message, bool isSent)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_UpdateMessageDet");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "Message", DbType.String, message);
                db.AddInParameter(cmd, "isSent", DbType.Boolean, isSent);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public DataSet getlocationmailid(int companyid, int locationid, bool training, bool hr, bool scm, bool compliance, bool operations)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetLocationwiseMailIDs");
                db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
                db.AddInParameter(cmd, "bTraining", DbType.Boolean, training);
                db.AddInParameter(cmd, "bhr", DbType.Boolean, hr);
                db.AddInParameter(cmd, "bscm", DbType.Boolean, scm);
                db.AddInParameter(cmd, "bcompliance", DbType.Boolean, compliance);
                db.AddInParameter(cmd, "boperations", DbType.Boolean, operations);

                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch
            {
                throw;
            }
        }


        #endregion

        private void ByteArrayToImage(byte[] byteArrayIn, string Path)
        {
            //System.Drawing.Image.GetThumbnailImageAbort myCallBack = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
            System.Drawing.Image newImage;


            if (byteArrayIn != null)
            {
                using (MemoryStream stream = new MemoryStream(byteArrayIn))
                {
                    //Bitmap newBitmap = new Bitmap(varBmp);
                    //varBmp.Dispose();
                    //varBmp = null;

                    newImage = System.Drawing.Image.FromStream(stream);

                    //var fileName = Path.GetFileName(fileurl.FileName);
                    //fileurl.SaveAs(Path.Combine(@"c:\projects", fileName));

                    newImage.Save(Path + "." + System.Drawing.Imaging.ImageFormat.Png);
                    newImage.Dispose();
                    newImage = null;


                }
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string uploadimages(string filename, string bas64Image, string guid, string deviceid)
        {
            byte[] data = null;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataTable dtmsg = new DataTable();
            dtmsg.Columns.Add("Status");
            dtmsg.Columns.Add("Message");
            dtmsg.Columns.Add("guid");
            dtmsg.Columns.Add("deviceid");
            try
            {
                objbasebo.UpdateLog("AddFeedBackDetails Start");


                // bas64Image = images[j].TrimStart('"').TrimEnd('"');
                bas64Image = bas64Image.TrimStart('"').TrimEnd('"');
                data = Convert.FromBase64String(bas64Image);


                if (data.Length > 0)
                {

                    System.Drawing.Image newImage;
                    filename = filename.TrimEnd(',').Replace('&', ' ').Replace('/', ' ');

                    if (data != null)
                    {
                        using (MemoryStream stream = new MemoryStream(data))
                        {
                            //Bitmap newBitmap = new Bitmap(varBmp);
                            //varBmp.Dispose();
                            //varBmp = null;

                            newImage = System.Drawing.Image.FromStream(stream);

                            //var fileName = Path.GetFileName(fileurl.FileName);
                            //fileurl.SaveAs(Path.Combine(@"c:\projects", fileName));

                            newImage.Save(ConfigurationManager.AppSettings["Auditimages"] + filename);
                            newImage.Dispose();
                            newImage = null;
                            dtmsg.Rows.Add("1", "Saved Sucessfully", guid, deviceid);

                            return ConvertJavaSeriptSerializer(dtmsg);


                        }
                    }
                    //ByteArrayToImage(data, ConfigurationManager.AppSettings["Feedbackimage"] + filename);
                }
            }

            catch
            {
                dtmsg.Rows.Add("0", "Not Saved Sucessfully", guid, deviceid);
                return ConvertJavaSeriptSerializer(dtmsg);
            }

            dtmsg.Rows.Add("2", "Un defined erro", guid, deviceid);
            return ConvertJavaSeriptSerializer(dtmsg);
        }





    }

    //public class Observations
    //{
    //    public string JS_SCM { get; set; }
    //    public string JS_Others { get; set; }
    //    public string JS_HR { get; set; }
    //    public string JS_Complaince { get; set; }
    //    public string JS_Training { get; set; }
    //    public string JS_Operation { get; set; }
    //}
}