﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;
using System.Xml;
using System.Text;
using Kowni.Common.BusinessLogic;
using System.Web.Script.Serialization;
using System.IO;
using System.Web.Script.Services;
using FAService.BaseBoClass;

using System.Net.Mail;
using Newtonsoft.Json;

using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Drawing;
using OfficeOpenXml.Drawing.Chart;
using System.Configuration;
using System.Globalization;



namespace FAService
{
    /// <summary>
    /// Summary description for FacilityAudit
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FacilityAudit : System.Web.Services.WebService
    {

        KCB.BLSingleSignOn objSingleSignOn = new KCB.BLSingleSignOn();
        KCB.BLAnnouncement objAnnouncement = new KCB.BLAnnouncement();
        BaseBO objbasebo = new BaseBO();

        KB.BLUser.BLUserCompany objuserCompany = new KB.BLUser.BLUserCompany();
        KB.BLUser.BLUserLocation objuserLocation = new KB.BLUser.BLUserLocation();

        [WebMethod]
        public string HelloWorld()
        {
            //DataSet dsRawdatas = GetRawDatasDateWiseReport(DateTime.Now, 2571, 5, 2);
            //string filename = Guid.NewGuid().ToString();
            //if (dsRawdatas.Tables[3].Rows.Count > 0)
            //{
            //    DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
            //    filename = drfilename["locsitename"].ToString();
            //}
            //string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";       

            //MakeExcel(dsRawdatas, "88.00", excelfilepath);

            //string CcMail = ",,,hemant.sahoo@dtss.in,jpatil@nse.co.in,ghanshyam.gaikwad@dtss.in,sanjeev.kumar@dtss.in,pinakin.solanki@dtss.in,sukhpreet.sidhu@dtss.in,,,sandeep.jagtap@dtss.in,suresh.gosavi@dtss.in,ghanshyam.gaikwad@dtss.in,rekha.prosper@dtss.in,madhavan.govindaraj@dtss.in,Pragati.bakshi@dtss.in,kunal.jasani@dtss.in,madhavan.govindaraj@dtss.in,stephenson.f@dtss.in,selvam.chokkappa@dtss.in,NHOperations@dtss.in,suresh.gosavi@dtss.in";
            //string[] emailaddress = CcMail.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
            //foreach (var address in emailaddress)
            //{
            //    if (address != string.Empty)
            //    {

            //    }
            //}


            //   SendMail(Convert.ToDateTime("22-11-2016"), 1207, 4034, 11, 2
            //, "Glasses of entire premises were not effectively cleaned as the rubber of glass cleaning applicator is not proper/new glass applicator to be provided at site. Spottings seen on the carpet were not effectively cleaned through weekend deep cleaning schedule. Workmen Register to be updated. Shortages of 2 staff out of 49 manpower to be filled ASAP."
            //, "N/A"
            //, "N/A"
            //, "ESIC cards not issued to new joined staff."
            //, "N/A"
            //, "N/A"
            //, "", 10, "89.22", "Accenture PDC4 - L246", "Audit reports and closers need to be shared. ");





            //SendMail(Convert.ToDateTime("28-11-2016"), 1207, 4496, 11, 2
            //, "Lack of ownership and poor monitoring observed within the supervisor's on maintaining the required standard at site. Grooming standard is not upto the mark and not being checked by the supervisors before deployment. Staff found updating deployment register is a serious concern. Effective cleaning is required for the entire glasses of premises. Spottings seen on the carpet were not effectively cleaned through weekend deep cleaning schedule. Effective cleaning is required in workstation area, reception area and wall spottings cleaning. Shortages of 2 staff out of 25 manpower to be filled ASPS as client has given deadline till 30th November 2016."
            //, "Supervisors required more training on their roles and responsibilities."
            //, "N/A"
            //, "N/A"
            //, "N/A"
            //, "N/A"
            //, "", 10, "85.81", "IBM 2nd and 4th Floor - L267", "More training required for supervisors to improve cleaning standard at site.");
            return "Hello World";
        }

        [WebMethod]
        public string GetLoginDetails(string username, string password)
        {
            //string memString = File.ReadAllText(Server.MapPath("AppThemes/images/datareport.html"));
            //// convert string to stream
            //byte[] buffer = Encoding.ASCII.GetBytes(memString);
            //MemoryStream ms = new MemoryStream(buffer);
            ////write to file
            //FileStream file = new FileStream("d:\\file.xls", FileMode.Create, FileAccess.Write);
            //ms.WriteTo(file);
            //file.Close();
            //ms.Close();

            DataSet ds = new DataSet();
            DataTable table = new DataTable("LoginDetails");
            string message = string.Empty;
            //try
            //{
            int RoleID = 0;
            int UserID = 0;
            int CompanyID = 0;
            string CompanyName = string.Empty;
            string firstname = string.Empty;
            string lastname = string.Empty;
            string emailid = string.Empty;
            string mobileno = string.Empty;
            int LocationID = 0;
            string LocationName = string.Empty;
            int GroupID = 0;
            int LanguageID = 0;
            string UserFName = string.Empty;
            string UserLName = string.Empty;
            string UserMailID = string.Empty;
            string ThemeFolderPath = string.Empty;



            if (!objSingleSignOn.Login(username, password, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
            {
                message = "Invalid User !";
                table.Columns.Add("status");
                table.Rows.Add("0");
            }
            else
            {
                table.Columns.Add("status");
                table.Columns.Add("userID");
                table.Columns.Add("CompanyID");
                table.Columns.Add("locationID");
                table.Columns.Add("companyName");
                table.Columns.Add("locationame");
                table.Columns.Add("firstname");
                table.Columns.Add("lastname");
                table.Columns.Add("emailid");
                table.Columns.Add("mobileno");
                table.Rows.Add(1, UserID, CompanyID, LocationID, CompanyName, LocationName, UserFName, UserLName, UserMailID);

            }
            //}
            //catch { }

            return ConvertJavaSeriptSerializer(table);
        }


        [WebMethod]
        public string getallsector(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                int id = 0;
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallsectorbyuserid");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Getcompanylogobyuserid(int companyid)
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                if (companyid != 0)
                {

                    Database db = DatabaseFactory.CreateDatabase("DBNameIs");
                    DbCommand cmd = db.GetStoredProcCommand("getlogobycompanyid");
                    db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                    ds = db.ExecuteDataSet(cmd);
                }



            }
            catch { }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }



        private string ConvertJavaSeriptSerializer(DataTable dt)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            List<object> resultMain = new List<object>();
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                foreach (DataColumn column in dt.Columns)
                {
                    result.Add(column.ColumnName, "" + row[column.ColumnName]);
                }
                resultMain.Add(result);
            }
            return serializer.Serialize(resultMain);
        }

        [WebMethod]
        public string getallsbu(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_allsbu");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);

                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string GetCompanybyuserid(int userid)
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                if (userid != 0)
                {
                    //ds = objuserCompany.GetUserCompany(Convert.ToInt32(userid));

                    //DataTable dFiltered = ds.Tables[0].Clone();
                    //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
                    //foreach (DataRow dr in dRowUpdates)
                    //    dFiltered.ImportRow(dr);

                    //ds.Tables.Clear();
                    //ds.Tables.Add(dFiltered);
                    Database db = DatabaseFactory.CreateDatabase("DBNameIs");
                    DbCommand cmd = db.GetStoredProcCommand("sp_Getcompanybyuserid");
                    db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                    ds = db.ExecuteDataSet(cmd);
                    DataTable dFiltered = ds.Tables[0].Clone();
                    DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
                    foreach (DataRow dr in dRowUpdates)
                        dFiltered.ImportRow(dr);

                    ds.Tables.Clear();
                    ds.Tables.Add(dFiltered);

                }
            }
            catch { }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod]
        public string GetLocationsbyuserid(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (userid != 0)
                {

                    Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                    DbCommand cmd = db.GetStoredProcCommand("And_getlocationbyuserid");
                    db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                    ds = db.ExecuteDataSet(cmd);


                }
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);
        }

        [WebMethod]
        public string getmaudit(int sectorid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getmaudit");
                db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);


        }

        [WebMethod]
        public string getallaudit(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallmaudit");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);


                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }



        [WebMethod]
        public string getallcategory(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallcategory");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);


                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallquestion(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallquestion");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod]
        public string getallscore(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallscore");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string gettransaction()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("gettransaction");
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);


        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetCriticalObservations()
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetAllCriticalObservations");
                ds = db.ExecuteDataSet(cmd);
            }
            catch { }
            return ConvertJavaSeriptSerializer_Dataset(ds);

        }


        private string ConvertJavaSeriptSerializer_Dataset(DataSet dsCritOp)
        {
            Dictionary<string, List<object>> lstresultMain = new Dictionary<string, List<object>>();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            //List<object> lstresultMain = new List<object>();
            foreach (DataTable dt in dsCritOp.Tables)
            {
                List<object> resultMain = new List<object>();
                foreach (DataRow row in dt.Rows)
                {
                    Dictionary<string, object> result = new Dictionary<string, object>();
                    foreach (DataColumn column in dt.Columns)
                    {
                        result.Add(column.ColumnName, "" + row[column.ColumnName]);
                    }
                    resultMain.Add(result);
                }
                lstresultMain.Add(dt.Columns[0].ColumnName, resultMain);
            }
            return serializer.Serialize(lstresultMain);
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetFeedbackQuestions()
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetClientFeedbackQuestions");
                ds = db.ExecuteDataSet(cmd);
            }
            catch { }
            return ConvertJavaSeriptSerializer_Dataset(ds);

        }



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string InsertTransaction(int sbuid, int companyid, int locationid, int sectorid, string XMLIs, string clientname, string sitename,
                                        string ssano, string clientperson, string sbuname, string aomname, string auditorname, string auditeename,
                                       string deviceID, string guiD, int userid, string auditdate, string auditorsignature, string clientsignature, string observation, string feedback,
            string Operations, string Training, string Scm, string Compliance, string hr, string Others, string followup, string totalScore)
        {

            // objbasebo.UpdateLog("XML : " + XMLIs);

            //XMLIs = "<root><facilityaudit  auditid='1' categoryid='1' questionid='1' scoreid='3' uploadfilename='' uploadfileGUID='43be8787-52d8-4f7d-8dc7-859549fb1345' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='2' scoreid='3' uploadfilename='' uploadfileGUID='4164fe13-fe92-4263-968a-758a5f8c3478' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='3' scoreid='3' uploadfilename='' uploadfileGUID='9b713dbe-d068-44b3-b1ae-351a9f644ca6' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='4' scoreid='3' uploadfilename='' uploadfileGUID='0b0bfe6a-2f95-4207-88d9-caae41d82cc6' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='5' scoreid='3' uploadfilename='' uploadfileGUID='6e9e4d43-c7c1-401c-8a28-a17604e05b1c' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='6' scoreid='3' uploadfilename='' uploadfileGUID='582a045f-3248-44d0-9516-5767d621bf9a' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='7' scoreid='3' uploadfilename='' uploadfileGUID='fadc98ff-e46b-4040-a285-62b4be5feb07' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='8' scoreid='3' uploadfilename='' uploadfileGUID='d5791d3d-93d7-4631-bc8f-24f2f91f42a2' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='9' scoreid='3' uploadfilename='' uploadfileGUID='5d6d603f-86eb-4ed1-9c1e-4aaa2da71ceb' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='10' scoreid='3' uploadfilename='' uploadfileGUID='ee1c912e-4ffb-45ab-a20f-a477b6b9e8f1' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='11' scoreid='3' uploadfilename='' uploadfileGUID='09136497-0a21-4758-b555-42f62490b64a' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='12' scoreid='3' uploadfilename='' uploadfileGUID='49c489b1-b14d-40bb-8f98-48ca7dc4e31e' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='13' scoreid='3' uploadfilename='' uploadfileGUID='229f7d53-bedc-4693-ae81-bcd98ff9f3e9' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='14' scoreid='3' uploadfilename='' uploadfileGUID='e4492528-e12a-4b82-97d9-2dac14b644fd' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='15' scoreid='3' uploadfilename='' uploadfileGUID='d53c19f1-1d26-4bb0-a671-027b437b17dd' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='16' scoreid='3' uploadfilename='' uploadfileGUID='b07bb630-9f42-4551-b6eb-391616fc82de' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='18' scoreid='1' uploadfilename='' uploadfileGUID='2b0ba8c6-5ca3-495f-9cf3-a93c4c146910' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='19' scoreid='1' uploadfilename='' uploadfileGUID='d3149508-f8d9-46a4-8a64-93ba14cf22a8' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='20' scoreid='1' uploadfilename='' uploadfileGUID='5f59f599-02a5-4503-9638-7cbec7864ac2' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='21' scoreid='2' uploadfilename='' uploadfileGUID='07184a2a-86d3-4812-a79f-53fde01ffddd' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='22' scoreid='1' uploadfilename='' uploadfileGUID='6c75005e-a23c-417a-b571-be9357e35c79' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='23' scoreid='1' uploadfilename='' uploadfileGUID='7c02e381-a892-43d2-9e1d-4c7cee84e571' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='24' scoreid='2' uploadfilename='' uploadfileGUID='2df14a0e-8baa-47ac-96c0-c4fe8ca4e4a4' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='25' scoreid='3' uploadfilename='' uploadfileGUID='9ce5c908-431a-43b7-94c0-edd87573cc8b' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='26' scoreid='3' uploadfilename='' uploadfileGUID='8621d9bd-0928-4e27-84d3-7560a3755161' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='27' scoreid='3' uploadfilename='' uploadfileGUID='a8d04d40-0ec9-4f2d-8bfa-f0f6d255dcf8' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='28' scoreid='3' uploadfilename='' uploadfileGUID='13d64800-35bc-4333-a76c-55a9e2f73dae' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='29' scoreid='3' uploadfilename='' uploadfileGUID='f98607df-d75c-4d35-a276-ed128dde0f1b' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='30' scoreid='3' uploadfilename='' uploadfileGUID='9578b89f-b0de-42c0-9f2e-9bc2b7ca0baf' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='31' scoreid='1' uploadfilename='' uploadfileGUID='2f375c37-a6cf-4a5c-815f-5f6615008672' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='32' scoreid='1' uploadfilename='' uploadfileGUID='b93d2f64-3764-4676-8dbd-b83d649aefcd' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='33' scoreid='2' uploadfilename='' uploadfileGUID='1e2a33fd-e930-4abd-bc7f-8ddb17cc9eef' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='34' scoreid='1' uploadfilename='' uploadfileGUID='2562716a-94d3-4513-93c5-521e094df431' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='35' scoreid='1' uploadfilename='' uploadfileGUID='804aeb85-f067-4e80-b480-4dcf566e8d74' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='36' scoreid='1' uploadfilename='' uploadfileGUID='fd66ece2-16a2-4c95-abc0-e2b04b6cdd29' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='37' scoreid='1' uploadfilename='' uploadfileGUID='09c97195-e81f-425c-b31c-ece8de4b1151' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='38' scoreid='1' uploadfilename='' uploadfileGUID='9be08bb8-cab7-43de-b1d6-b8403952df34' remarks='' />,<facilityaudit  auditid='1' categoryid='5' questionid='39' scoreid='1' uploadfilename='' uploadfileGUID='88473d79-6fdc-4cfd-8ffb-eda398aa564f' remarks='' />,<facilityaudit  auditid='1' categoryid='5' questionid='40' scoreid='2' uploadfilename='' uploadfileGUID='015fe2c7-2226-4e3d-af6c-3a2a80baca33' remarks='' />,<facilityaudit  auditid='1' categoryid='5' questionid='41' scoreid='1' uploadfilename='' uploadfileGUID='7983898c-ab74-4666-b75a-a16342d3cab9' remarks='' />,<facilityaudit  auditid='1' categoryid='6' questionid='42' scoreid='1' uploadfilename='' uploadfileGUID='fddda712-bc4e-4243-9b5f-ccbb36e02b91' remarks='' />,<facilityaudit  auditid='1' categoryid='6' questionid='43' scoreid='1' uploadfilename='' uploadfileGUID='e6b6161e-22cf-414e-8f7d-aeed5f22b740' remarks='' />,<facilityaudit  auditid='1' categoryid='7' questionid='44' scoreid='1' uploadfilename='' uploadfileGUID='17595d2f-78c1-4677-9181-f511f8b3158d' remarks='' />,<facilityaudit  auditid='1' categoryid='7' questionid='45' scoreid='1' uploadfilename='' uploadfileGUID='ea77f562-cd99-40fe-b4f0-4b4b2b501fb4' remarks='' />,<facilityaudit  auditid='1' categoryid='7' questionid='46' scoreid='2' uploadfilename='' uploadfileGUID='b56d6ae3-6a4b-4ab3-8afe-91448566be04' remarks='' />,<facilityaudit  auditid='1' categoryid='7' questionid='47' scoreid='1' uploadfilename='' uploadfileGUID='d0765c39-3b8a-4630-8264-5d81b090c0a2' remarks='' />,<facilityaudit  auditid='1' categoryid='8' questionid='48' scoreid='1' uploadfilename='' uploadfileGUID='11c70137-3bff-42a5-a955-158bd9bec22f' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='49' scoreid='25' uploadfilename='' uploadfileGUID='312bb665-356e-410c-84ca-91a166f0dd79' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='50' scoreid='26' uploadfilename='' uploadfileGUID='7777f0f6-9083-475f-b442-43192ba40ab5' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='51' scoreid='25' uploadfilename='' uploadfileGUID='aeca719e-2149-46ee-972e-4147d2e1365d' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='52' scoreid='25' uploadfilename='' uploadfileGUID='90209beb-0469-4b91-b499-e32bdb3ad605' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='53' scoreid='25' uploadfilename='' uploadfileGUID='4353a8db-7f05-4785-9bd3-c25843e074e6' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='54' scoreid='26' uploadfilename='' uploadfileGUID='fc0523a9-d807-4f5e-b9f9-f6266f0b912d' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='55' scoreid='25' uploadfilename='' uploadfileGUID='79517a89-6c34-4aa7-924a-9a8d5880b6fa' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='56' scoreid='26' uploadfilename='' uploadfileGUID='ef0b97a5-e439-4404-887f-815707046855' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='57' scoreid='25' uploadfilename='' uploadfileGUID='2dba0207-e33a-4692-afa5-3d9a472ab184' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='58' scoreid='27' uploadfilename='' uploadfileGUID='ff0b0858-3c20-4916-91cd-68a3e6cf7d16' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='59' scoreid='27' uploadfilename='' uploadfileGUID='cf7e5f2c-ec1b-4bc7-82ed-ee029739140c' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='60' scoreid='27' uploadfilename='' uploadfileGUID='fbfc5508-9c3b-483d-afb7-25f9a6faccca' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='61' scoreid='27' uploadfilename='' uploadfileGUID='197cd0c7-571d-4eda-8a30-464ca9e4b29a' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='62' scoreid='27' uploadfilename='' uploadfileGUID='5179d3ef-3312-49b4-9893-d6f6b88b5721' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='63' scoreid='27' uploadfilename='' uploadfileGUID='e8004c1d-a654-4944-8eb3-01d3c8292724' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='64' scoreid='27' uploadfilename='' uploadfileGUID='a8b98d82-4d9b-4c44-bc75-48c1db229d40' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='65' scoreid='27' uploadfilename='' uploadfileGUID='62e7d988-addc-475a-af07-8bf837735a3d' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='66' scoreid='27' uploadfilename='' uploadfileGUID='9386eb0c-45c7-4cdf-88d1-a0432046633f' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='67' scoreid='27' uploadfilename='' uploadfileGUID='4ca71989-42fd-447f-9de7-572157498c9c' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='68' scoreid='27' uploadfilename='' uploadfileGUID='a5a48a7f-f66a-4ae6-8201-be8daf38fdda' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='69' scoreid='25' uploadfilename='' uploadfileGUID='1121e07b-68f1-4b71-8e15-8be4d5b86722' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='70' scoreid='25' uploadfilename='' uploadfileGUID='8ffd2067-208e-461c-bbae-92166d351f90' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='71' scoreid='25' uploadfilename='' uploadfileGUID='c09e93b0-84d7-4bd1-9bbb-e78abfc3a1fd' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='72' scoreid='26' uploadfilename='' uploadfileGUID='dbdbad5a-18f0-424b-bd00-a70f091092d5' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='73' scoreid='25' uploadfilename='' uploadfileGUID='14cabb4c-f66b-495d-b73b-34852c868a31' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='74' scoreid='25' uploadfilename='' uploadfileGUID='4cfbbb2b-49df-40ca-9d78-65f626abfbd2' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='75' scoreid='27' uploadfilename='' uploadfileGUID='411fd35c-da4d-4ea8-95a5-0296958c463e' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='76' scoreid='27' uploadfilename='' uploadfileGUID='9d79f7ca-f72b-4bc3-9cdf-3bb92686db50' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='77' scoreid='27' uploadfilename='' uploadfileGUID='0adc5a35-3d4a-44bc-923f-6ea939103406' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='78' scoreid='27' uploadfilename='' uploadfileGUID='8ce2ef8f-4d77-4776-b414-b48fe19a3f97' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='79' scoreid='27' uploadfilename='' uploadfileGUID='bdb00f07-5e72-4553-a864-9f4a363eb5a5' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='80' scoreid='27' uploadfilename='' uploadfileGUID='d6b98352-13fc-427b-862c-68c52cc8ceaf' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='81' scoreid='27' uploadfilename='' uploadfileGUID='0a1162e3-3479-47c7-88df-8f3db416624b' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='82' scoreid='27' uploadfilename='' uploadfileGUID='32b7638d-50ed-44f6-bb6b-7441e838a4a8' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='83' scoreid='27' uploadfilename='' uploadfileGUID='5b50c6c8-af14-4efe-8368-ccc5c4eb3ced' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='84' scoreid='27' uploadfilename='' uploadfileGUID='50587cbc-6869-4120-930b-ee1276041deb' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='85' scoreid='27' uploadfilename='' uploadfileGUID='2fa2c092-1c69-4d02-9fb4-9d5c8c9337a4' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='86' scoreid='27' uploadfilename='' uploadfileGUID='c98f7069-3e83-484d-a39a-b6b1ea3b481d' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='87' scoreid='25' uploadfilename='' uploadfileGUID='23042815-e9d7-4f0a-a824-60bdddd3f0c1' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='88' scoreid='25' uploadfilename='' uploadfileGUID='8388cf6d-41af-4b44-b485-b134b2693e3c' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='89' scoreid='25' uploadfilename='' uploadfileGUID='7026b0c2-d781-4720-8891-ac41e5ab3377' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='90' scoreid='25' uploadfilename='' uploadfileGUID='a882dc4f-8fe3-4dd5-82e1-a3bbbae07918' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='91' scoreid='26' uploadfilename='' uploadfileGUID='5ae338ea-c572-4c05-af1d-46d9f617f534' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='92' scoreid='25' uploadfilename='' uploadfileGUID='c6e437f6-e48b-42bc-845c-571599f6e9bf' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='93' scoreid='25' uploadfilename='' uploadfileGUID='e7679054-f1df-4b89-a981-245450569cae' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='94' scoreid='26' uploadfilename='' uploadfileGUID='f4bea368-5f6d-42b3-a88f-6a18a426b4f3' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='95' scoreid='25' uploadfilename='' uploadfileGUID='b7ea7f75-6949-4ff5-ba3c-7f1c171827d8' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='96' scoreid='25' uploadfilename='' uploadfileGUID='6975673b-f2a9-488c-9bd6-73c02418edbf' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='97' scoreid='25' uploadfilename='' uploadfileGUID='174b8743-f921-4650-825a-58ab119f6114' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='98' scoreid='25' uploadfilename='' uploadfileGUID='4b94b089-a2c4-455e-a9f2-e1701147cc46' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='99' scoreid='25' uploadfilename='' uploadfileGUID='cd9f17d7-f3a3-4082-9615-6bb4f72cc16d' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='100' scoreid='25' uploadfilename='' uploadfileGUID='673e1042-052f-480e-8811-88bd20ea29a5' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='101' scoreid='25' uploadfilename='' uploadfileGUID='5c43dc0c-658b-4ea6-b4c6-18010110c070' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='102' scoreid='26' uploadfilename='' uploadfileGUID='45e27e36-d741-4f36-b9fa-d070aa736e5c' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='103' scoreid='25' uploadfilename='' uploadfileGUID='2335ba83-a15d-4493-92e8-e0b441ba53ab' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='104' scoreid='25' uploadfilename='' uploadfileGUID='dfbf2a86-a5b3-4cca-b00c-e0315ee95929' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='105' scoreid='26' uploadfilename='' uploadfileGUID='71ed4c50-3f77-44be-be18-8d38ed360fa4' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='106' scoreid='25' uploadfilename='' uploadfileGUID='020bf31c-6930-4bf4-b3b7-3d7d89141eaa' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='107' scoreid='26' uploadfilename='' uploadfileGUID='976a030e-5f62-4a2c-9910-167c2343dda9' remarks='' />,<facilityaudit  auditid='2' categoryid='15' questionid='108' scoreid='27' uploadfilename='' uploadfileGUID='9fe8a4c2-fed4-43b2-82c2-1e824385df74' remarks='' />,<facilityaudit  auditid='2' categoryid='15' questionid='109' scoreid='27' uploadfilename='' uploadfileGUID='bbeebbd6-53b0-43c6-805d-0ef117af2864' remarks='' />,<facilityaudit  auditid='2' categoryid='15' questionid='110' scoreid='27' uploadfilename='' uploadfileGUID='73c545f2-4c2f-4687-ac8e-79ff73c04ec9' remarks='' />,<facilityaudit  auditid='2' categoryid='16' questionid='111' scoreid='25' uploadfilename='' uploadfileGUID='a1001f13-26dc-422c-aa65-6579c1f899be' remarks='' />,<facilityaudit  auditid='2' categoryid='16' questionid='112' scoreid='26' uploadfilename='' uploadfileGUID='38e21c54-fe1c-4cbe-ad21-6830991bee34' remarks='' />,<facilityaudit  auditid='2' categoryid='16' questionid='113' scoreid='25' uploadfilename='' uploadfileGUID='0e0adf0a-23da-49ec-9b0d-ae0957c69763' remarks='' />,<facilityaudit  auditid='2' categoryid='16' questionid='114' scoreid='26' uploadfilename='' uploadfileGUID='31e58746-9a70-4e37-b9ad-b0687a55e66a' remarks='' />,<facilityaudit  auditid='2' categoryid='17' questionid='115' scoreid='25' uploadfilename='' uploadfileGUID='7b66b893-d9d0-426c-88c9-c1e1b26ddea0' remarks='' />,<facilityaudit  auditid='2' categoryid='17' questionid='116' scoreid='26' uploadfilename='' uploadfileGUID='e51af556-b60e-4234-b59f-9c14cfca0c36' remarks='' />,<facilityaudit  auditid='2' categoryid='17' questionid='117' scoreid='25' uploadfilename='' uploadfileGUID='22aec369-8b43-4f93-9801-1a900b6e5282' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='118' scoreid='25' uploadfilename='' uploadfileGUID='d5b508c3-760f-47ce-a7cd-b265c7cd215d' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='119' scoreid='26' uploadfilename='' uploadfileGUID='8d6f7576-60bd-45f5-8814-37bd8771baf8' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='120' scoreid='25' uploadfilename='' uploadfileGUID='22dab489-c842-4a48-ad82-df55ed50f378' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='121' scoreid='26' uploadfilename='' uploadfileGUID='f436c32c-03c2-4aea-ae8b-a5caa931ebf2' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='122' scoreid='25' uploadfilename='' uploadfileGUID='daeace33-73d3-488c-9ae1-134702601e3e' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='123' scoreid='26' uploadfilename='' uploadfileGUID='6a8fe127-7ed1-41de-b97f-3ff684bf9917' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='124' scoreid='25' uploadfilename='' uploadfileGUID='aaac3c3d-430d-4e10-943a-3b8db4b372ff' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='125' scoreid='26' uploadfilename='' uploadfileGUID='11d53b3d-042b-465b-96e5-2b78e42cf2e2' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='126' scoreid='25' uploadfilename='' uploadfileGUID='d576a4bb-2536-4cb4-b961-c10dfb1ec466' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='127' scoreid='25' uploadfilename='' uploadfileGUID='d514b4a7-5df5-473e-a8a2-24b366ebb951' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='128' scoreid='26' uploadfilename='' uploadfileGUID='d1d42c1d-8f92-444a-b348-0b8c14f82fe5' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='129' scoreid='25' uploadfilename='' uploadfileGUID='baacad4b-e73d-416d-8897-c2e1d8d4d8bb' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='130' scoreid='25' uploadfilename='' uploadfileGUID='a330a96d-f2d3-4307-9709-ace6959f03de' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='131' scoreid='25' uploadfilename='' uploadfileGUID='c574783b-f855-4340-9f05-c0c521c6c7b4' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='132' scoreid='26' uploadfilename='' uploadfileGUID='79a14529-d7c8-403a-9dee-094c6ab0d238' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='133' scoreid='25' uploadfilename='' uploadfileGUID='2e66a566-a554-47ea-8472-b0c3896afc93' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='134' scoreid='26' uploadfilename='' uploadfileGUID='95b2504a-2e52-4c21-b8c6-41f1d87a6f24' remarks='' />,<facilityaudit  auditid='2' categoryid='20' questionid='135' scoreid='25' uploadfilename='' uploadfileGUID='f95eb1c0-c1e9-4321-b18c-04d9e99d97a9' remarks='' />,<facilityaudit  auditid='2' categoryid='20' questionid='136' scoreid='26' uploadfilename='' uploadfileGUID='5b969883-5418-4d7c-9cef-e2edde30e3c9' remarks='' />,<facilityaudit  auditid='2' categoryid='20' questionid='137' scoreid='25' uploadfilename='' uploadfileGUID='1fb31acf-61af-4b0d-841e-b4ff7cc9d300' remarks='' />,<facilityaudit  auditid='2' categoryid='20' questionid='138' scoreid='25' uploadfilename='' uploadfileGUID='60073c9a-aadf-473d-a022-e20fae65522f' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='140' scoreid='25' uploadfilename='' uploadfileGUID='be587abc-3f85-4114-9be1-50f08de4d856' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='141' scoreid='26' uploadfilename='' uploadfileGUID='063df69d-e26a-4fcf-9ccc-1f4d7d64efe8' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='142' scoreid='25' uploadfilename='' uploadfileGUID='15b8d553-3873-465d-b740-504d2c9e85ae' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='143' scoreid='25' uploadfilename='' uploadfileGUID='d938c36b-56b4-4c4e-9e00-82994a4f31ff' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='144' scoreid='25' uploadfilename='' uploadfileGUID='650869da-3580-4a92-93a2-e5ace842a145' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='145' scoreid='27' uploadfilename='' uploadfileGUID='72267c7d-c5f4-4f98-adfd-e322b8087a8a' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='146' scoreid='27' uploadfilename='' uploadfileGUID='20c7c4fd-2b2e-4fca-8c12-289a304c3bc3' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='147' scoreid='27' uploadfilename='' uploadfileGUID='25b852c7-378a-4d11-83ff-e1ae841da0be' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='148' scoreid='27' uploadfilename='' uploadfileGUID='bef71d9b-9510-425c-a91f-8c40b705c66b' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='149' scoreid='27' uploadfilename='' uploadfileGUID='2ab304dc-70de-41b6-bbb5-889247626564' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='150' scoreid='27' uploadfilename='' uploadfileGUID='d2b79b72-7366-410b-be32-c266da4d5bc5' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='151' scoreid='27' uploadfilename='' uploadfileGUID='dfecadf9-a975-4efc-8fc4-c994627f4c83' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='152' scoreid='27' uploadfilename='' uploadfileGUID='3fbfbf7d-c1ea-4366-bf0c-8270547696bf' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='153' scoreid='27' uploadfilename='' uploadfileGUID='9bfe3ba7-99b1-464d-b0a4-8215ede9dbef' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='154' scoreid='27' uploadfilename='' uploadfileGUID='7ae108a0-8758-478c-831e-1039bf92095d' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='155' scoreid='27' uploadfilename='' uploadfileGUID='d8751483-f855-4190-8e7f-64ff850d839c' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='156' scoreid='27' uploadfilename='' uploadfileGUID='fb562833-be6e-4d53-b172-a515e373e46a' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='157' scoreid='27' uploadfilename='' uploadfileGUID='9af5c07e-9d47-4480-862e-9e9e84970d0a' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='158' scoreid='27' uploadfilename='' uploadfileGUID='8d787c21-c06c-4dbc-8050-65c01afb8104' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='159' scoreid='27' uploadfilename='' uploadfileGUID='9b174751-f55b-48ae-90ab-4cbfa88b1545' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='160' scoreid='27' uploadfilename='' uploadfileGUID='1c96750f-a15d-4464-8a72-ec0813225601' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='161' scoreid='27' uploadfilename='' uploadfileGUID='3654a340-c90b-4330-be27-ae36dac8e0ff' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='162' scoreid='27' uploadfilename='' uploadfileGUID='7e4275f0-f3a1-4de5-aef9-2ac91bbfd0a2' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='163' scoreid='27' uploadfilename='' uploadfileGUID='c469207d-6e2a-41da-8c0f-2722d9570afd' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='164' scoreid='27' uploadfilename='' uploadfileGUID='8e727ff7-eec6-4cc1-96ec-2ad9138d350a' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='165' scoreid='27' uploadfilename='' uploadfileGUID='047750cc-cdca-433e-9d30-087bb4a52d0e' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='166' scoreid='27' uploadfilename='' uploadfileGUID='dff32ebe-b849-40f1-a390-5c9651eeba86' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='167' scoreid='27' uploadfilename='' uploadfileGUID='3f17dc08-dc7a-4709-84a0-7bb674f74cf9' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='168' scoreid='27' uploadfilename='' uploadfileGUID='80ecb644-86e7-4f4b-b04a-f00e8b5f2f15' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='169' scoreid='27' uploadfilename='' uploadfileGUID='a045556e-8bf7-4b4f-aed7-dd8b21816f65' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='170' scoreid='27' uploadfilename='' uploadfileGUID='b6cbc190-9681-47c0-a4ba-f0d922f7541d' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='171' scoreid='27' uploadfilename='' uploadfileGUID='9f6cbf03-3a81-4d8a-a84f-3eff0010b0a0' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='172' scoreid='27' uploadfilename='' uploadfileGUID='67035dc8-5812-4743-9b87-e61d21b910d1' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='173' scoreid='27' uploadfilename='' uploadfileGUID='3f49b87b-3834-460d-ac76-6cc4ce78fc92' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='174' scoreid='27' uploadfilename='' uploadfileGUID='50f22d02-aa3f-4d65-b51c-d3309c907b24' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='175' scoreid='27' uploadfilename='' uploadfileGUID='776037a7-7b53-423f-886a-9c2f9d76347d' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='176' scoreid='27' uploadfilename='' uploadfileGUID='94958601-5188-4613-afd7-47c7f4cb0d89' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='177' scoreid='27' uploadfilename='' uploadfileGUID='91340d9e-cd4d-4a79-a814-f77ba20ca299' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='178' scoreid='27' uploadfilename='' uploadfileGUID='e180dcf2-db80-4d14-8e20-1b17da9546c6' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='179' scoreid='27' uploadfilename='' uploadfileGUID='d9fcabec-8032-489e-afb9-551f8bf450c0' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='180' scoreid='27' uploadfilename='' uploadfileGUID='6a72f18b-e06f-4239-8c68-98b534f3b256' remarks='' /></root>";

            DataSet ds2 = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("CheckAduittransaction");
            db.AddInParameter(cmd, "deviceID", DbType.String, deviceID);
            db.AddInParameter(cmd, "guiD", DbType.String, guiD);
            ds2 = db.ExecuteDataSet(cmd);


            if (ds2 != null && ds2.Tables.Count > 0 && ds2.Tables[0].Rows.Count > 0)
            {
                return ConvertJavaSeriptSerializer(ds2.Tables[0]);
            }
            else
            {

                #region "New"

                try
                {
                    DateTime auddate = Convert.ToDateTime(auditdate);
                    DataSet dsTransaction = new DataSet();
                    DbCommand cmdTransaction = db.GetStoredProcCommand("Qry_CheckTransaction");
                    db.AddInParameter(cmdTransaction, "@LocationId", DbType.Int32, locationid);
                    db.AddInParameter(cmdTransaction, "@AuditDate", DbType.DateTime, auddate);
                    dsTransaction = db.ExecuteDataSet(cmdTransaction);
                    if (dsTransaction != null && dsTransaction.Tables.Count > 0 && dsTransaction.Tables[0].Rows.Count > 0)
                    {
                        return ConvertJavaSeriptSerializer(dsTransaction.Tables[0]);
                    }

                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    DataSet dtinspect = new DataSet();
                    DataTable dlinspect = new DataTable();
                    StringBuilder objbuilder = new StringBuilder();

                    //"21:09:2015 13:54:12 PM"
                    //string[] fdate = XMLIs.Split(':');
                    //XMLIs = fdate[1].ToString() + "/" + fdate[0].ToString() + "/" + fdate[2].ToString() + ":" + fdate[3].ToString() + ":" + fdate[4].ToString();

                    byte[] auditorsign = null;
                    byte[] clientsign = null;
                    string auditorguid = "";
                    string clientguid = "";
                    try
                    {
                        string bas64Image = auditorsignature;
                        if (bas64Image.Trim() != string.Empty)
                        {
                            auditorsign = Convert.FromBase64String(bas64Image);
                            if (auditorsign.Length > 0)
                            {
                                auditorguid = Guid.NewGuid().ToString();
                                ByteArrayToImage(auditorsign, ConfigurationManager.AppSettings["auditorsignature"] + auditorguid.Replace('/', ' '));
                                auditorguid = auditorguid + ".png";
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        return serializer.Serialize(e.Message);
                    }

                    try
                    {
                        string bas64Image = clientsignature;
                        if (bas64Image.Trim() != string.Empty)
                        {
                            clientsign = Convert.FromBase64String(bas64Image);
                            if (clientsign.Length > 0)
                            {
                                clientguid = Guid.NewGuid().ToString();
                                ByteArrayToImage(clientsign, ConfigurationManager.AppSettings["clientsignature"] + clientguid.Replace('/', ' '));
                                clientguid = clientguid + ".png";
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return serializer.Serialize(e.Message);
                    }




                    DbCommand cmmd = db.GetStoredProcCommand("addmaudittransaction_New");
                    DataTable dtmsg = new DataTable();
                    dtmsg.Columns.Add("Status");
                    dtmsg.Columns.Add("Message");
                    dtmsg.Columns.Add("guid");
                    dtmsg.Columns.Add("deviceid");

                    //objbasebo.UpdateLog("Before XML - Add");
                    //Addxml(XMLIs);
                    XMLIs = XMLIs.Replace("&", "&amp;");
                    db.AddInParameter(cmmd, "sbuid", DbType.Int32, sbuid);
                    db.AddInParameter(cmmd, "companyid", DbType.Int32, companyid);
                    db.AddInParameter(cmmd, "locationid", DbType.Int32, locationid);
                    db.AddInParameter(cmmd, "sectorid", DbType.Int32, sectorid);
                    db.AddInParameter(cmmd, "XMLIs", DbType.String, XMLIs);

                    //objbasebo.UpdateLog("After XML - Add");

                    db.AddInParameter(cmmd, "clientname", DbType.String, clientname);
                    db.AddInParameter(cmmd, "sitename", DbType.String, sitename);
                    db.AddInParameter(cmmd, "ssano", DbType.String, ssano);
                    db.AddInParameter(cmmd, "clientperson", DbType.String, clientperson);
                    db.AddInParameter(cmmd, "sbuname", DbType.String, sbuname);
                    db.AddInParameter(cmmd, "aomname", DbType.String, aomname);
                    db.AddInParameter(cmmd, "auditorname", DbType.String, auditorname);
                    db.AddInParameter(cmmd, "auditeename", DbType.String, auditeename);
                    db.AddInParameter(cmmd, "deviceID", DbType.String, deviceID);
                    db.AddInParameter(cmmd, "guiD", DbType.String, guiD);
                    db.AddInParameter(cmmd, "userid", DbType.Int32, userid);
                    db.AddInParameter(cmmd, "auddate", DbType.DateTime, auddate);
                    db.AddInParameter(cmmd, "auditorguid", DbType.String, auditorguid);
                    db.AddInParameter(cmmd, "clientguid", DbType.String, clientguid);
                    db.AddInParameter(cmmd, "observation", DbType.String, observation);
                    db.AddInParameter(cmmd, "feedback", DbType.String, feedback);

                    db.AddInParameter(cmmd, "CrticalOperations", DbType.String, Operations);
                    db.AddInParameter(cmmd, "CrticalTraining", DbType.String, Training);
                    db.AddInParameter(cmmd, "CrticalSCM", DbType.String, Scm);
                    db.AddInParameter(cmmd, "CrticalCompliance", DbType.String, Compliance);
                    db.AddInParameter(cmmd, "CrticalHR", DbType.String, hr);
                    db.AddInParameter(cmmd, "CrticalOthers", DbType.String, Others);

                    db.AddInParameter(cmmd, "FinalScore", DbType.String, totalScore);
                    string sfollowupdate = string.Empty;
                    if (followup.Trim() != string.Empty)
                    {
                        db.AddInParameter(cmmd, "FollowUpDate", DbType.DateTime, Convert.ToDateTime(followup));
                        sfollowupdate = Convert.ToDateTime(followup).ToShortDateString();
                    }
                    else
                    {
                        db.AddInParameter(cmmd, "FollowUpDate", DbType.DateTime, DBNull.Value);
                        sfollowupdate = string.Empty;
                    }

                    Decimal dTotalScore = Convert.ToDecimal(totalScore);
                    int iClosureDate = 0;
                    if (dTotalScore >= 83)
                    {
                        iClosureDate = 10;
                    }
                    else
                    {
                        iClosureDate = 7;
                    }
                    db.AddInParameter(cmmd, "AuditClosureDate", DbType.Int32, iClosureDate);

                    string[] Values = new string[3];
                    db.AddOutParameter(cmmd, "@ErrorMessage", DbType.String, 1000);
                    db.AddOutParameter(cmmd, "@Status", DbType.String, 1000);
                    db.ExecuteNonQuery(cmmd);
                    Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                    db.GetParameterValue(cmmd, "@ErrorMessage"));
                    Values[1] = db.GetParameterValue(cmmd, "@Status").ToString();

                    dtmsg.Rows.Add("1", "Savedsucessfully", guiD, deviceID);
                    if (Values[1] != "-1")
                    {
                        try
                        {
                            objbasebo.UpdateLog("Mail Function Start");
                            SendMail(auddate, companyid, locationid, sbuid, sectorid, Operations, Training, Scm, Compliance, hr, Others, sfollowupdate, iClosureDate, totalScore, sbuname, feedback);
                            objbasebo.UpdateLog("Mail Function End");
                        }
                        catch { }
                    }
                    return ConvertJavaSeriptSerializer(dtmsg);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("PRIMARY KEY"))
                    {
                        DataTable dtmessage = new DataTable();
                        dtmessage.Columns.Add("deviceid");
                        dtmessage.Columns.Add("guid");
                        dtmessage.Columns.Add("Status");
                        dtmessage.Rows.Add(deviceID, guiD, 1);
                        return ConvertJavaSeriptSerializer(dtmessage);
                    }
                    objbasebo.UpdateLog("AddFeedBackDetails Error" + e.Message.ToString());
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    return serializer.Serialize(e.Message);
                }
                #endregion
            }

        }

        private void Addxml(string xml)
        {
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmmd = db.GetStoredProcCommand("Addxml");
            db.AddInParameter(cmmd, "xml", DbType.String, xml);
            db.ExecuteNonQuery(cmmd);
        }

        #region "Split Function"

        private string GetSiteID(string sitename)
        {
            string[] sitedet = sitename.Split('-');
            return sitedet[sitedet.Length - 1];
        }

        #endregion

        #region "Send Mail For ClientWise  MailIDs"
        private void SendMail(DateTime auditdate, int companyid, int locationid, int sbuid, int sectorid, string operations, string training, string scm, string compliance, string hr, string others, string followupdate, int closuredate, string totalscore, string sbuname, string feedback)
        {
            try
            {
                DataSet dsRawdatas = GetRawDatasDateWiseReport(auditdate, locationid, sbuid, sectorid);
                objbasebo.UpdateLog("Row Count : " + dsRawdatas.Tables[0].Rows.Count);
                if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                {
                    string path = System.AppDomain.CurrentDomain.BaseDirectory + "AuditFiles";
                    //string filename = path + "\\" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";


                    string filename = Guid.NewGuid().ToString();
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                        filename = drfilename["locsitename"].ToString().Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty);
                    }

                    if (dsRawdatas.Tables[4].Rows.Count > 0)
                    {
                        totalscore = dsRawdatas.Tables[4].Rows[0][0].ToString();
                    }
                    string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                    objbasebo.UpdateLog("Work Book Path :" + excelfilepath);
                    //WriteToFile(filename);

                    //GenerateExcel(dsRawdatas, totalscore, filename);

                    MakeExcel(dsRawdatas, totalscore, excelfilepath);

                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sbuname = drHeader["locsitename"].ToString();

                    string bodymail = File.ReadAllText(Server.MapPath("AppThemes/images/MailTemplate.txt"), Encoding.UTF8);
                    bodymail = bodymail.Replace("#Operations#", operations);
                    bodymail = bodymail.Replace("#Training#", training);
                    bodymail = bodymail.Replace("#SCM#", scm);
                    bodymail = bodymail.Replace("#Compliance#", compliance);
                    bodymail = bodymail.Replace("#HR#", hr);
                    bodymail = bodymail.Replace("#Others#", others);
                    bodymail = bodymail.Replace("#Score#", totalscore + "%");
                    bodymail = bodymail.Replace("#Closure#", auditdate.AddDays(closuredate).ToShortDateString());
                    bodymail = bodymail.Replace("#SbuName#", sbuname);
                    bodymail = bodymail.Replace("#feedback#", feedback);

                    string subject = "Quality Audit - " + sbuname + " - " + auditdate.ToShortDateString();

                    if (followupdate.Trim() != string.Empty)
                    {
                        bodymail = bodymail.Replace("#Follow#", followupdate);
                    }
                    else
                    {
                        bodymail = bodymail.Replace("#Follow#", "Not Applicable");
                    }

                    SendingMail(bodymail, subject, excelfilepath, companyid, locationid, training, hr, scm, compliance, operations, auditdate, sbuid);
                    objbasebo.UpdateLog("Saved :" + path);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //private void SendObservationsMail(string operations, string training, string scm, string compliance, string hr, string others, int companyid, int locationid)
        //{
        //    try
        //    {
        //        DataSet dsEmailDet = getlocationmailid(companyid, locationid);
        //        if (dsEmailDet.Tables.Count > 0 && dsEmailDet.Tables[0].Rows.Count > 0)
        //        {
        //            DataRow drEmail = dsEmailDet.Tables[0].Rows[0];
        //            string operationheadname = drEmail["OperationHeadName"].ToString().Trim();
        //            string operationheadmailid = drEmail["OperationHeadMail"].ToString().Trim();

        //            string TrainingHeadName = drEmail["TrainingHeadName"].ToString().Trim();
        //            string TrainingHeadMail = drEmail["TrainingHeadMail"].ToString().Trim();

        //            string SCMHeadName = drEmail["SCMHeadName"].ToString().Trim();
        //            string SCMHeadMail = drEmail["SCMHeadMail"].ToString().Trim();

        //            string ComplianceHeadName = drEmail["ComplianceHeadName"].ToString().Trim();
        //            string ComplianceHeadMail = drEmail["ComplianceHeadMail"].ToString().Trim();

        //            string HRHeadName = drEmail["HRHeadName"].ToString().Trim();
        //            string HRHeadMail = drEmail["HRHeadMail"].ToString().Trim();


        //            string OthersHeadName = drEmail["OthersHeadName"].ToString().Trim();
        //            string OthersHeadMail = drEmail["OthersHeadMail"].ToString().Trim();

        //            string subject = string.Empty;
        //            string body = string.Empty;
        //            string ccmailids = string.Empty;
        //            if (operationheadmailid != string.Empty)
        //            {
        //                subject = "Critcal Observation - Operations";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, operationheadmailid, ccmailids);

        //            }
        //            if (TrainingHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - Training";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, TrainingHeadMail, ccmailids);
        //            }
        //            if (SCMHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - SCM";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, SCMHeadMail, ccmailids);
        //            }

        //            if (ComplianceHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - Compliance";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, ComplianceHeadMail, ccmailids);
        //            }

        //            if (HRHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - HR";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, HRHeadMail, ccmailids);
        //            }
        //            if (OthersHeadMail != string.Empty)
        //            {
        //                subject = "Critcal Observation - Others";
        //                //body parameters change
        //                //cc mailids
        //                MailObservations(body, subject, OthersHeadMail, ccmailids);
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        private void MailObservations(string body, string subject, string toMail, string ccmail)
        {
            try
            {
                string username = string.Empty;
                string password = string.Empty;
                string smtpServer = string.Empty;
                string frommail = string.Empty;
                int portno = 0;


                MailMessage mailMessage = new MailMessage();
                if (ConfigurationManager.AppSettings["UserName"] != null)
                {
                    username = ConfigurationManager.AppSettings["UserName"];
                }

                if (ConfigurationManager.AppSettings["Password"] != null)
                {
                    password = ConfigurationManager.AppSettings["Password"];
                }

                if (ConfigurationManager.AppSettings["SmtpServer"] != null)
                {
                    smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                }

                if (ConfigurationManager.AppSettings["Port"] != null)
                {
                    portno = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                }

                if (ConfigurationManager.AppSettings["FromMail"] != null)
                {
                    frommail = ConfigurationManager.AppSettings["FromMail"];
                }

                //usermail = "venkadesan.n@i2isoftwares.com";
                //ccmail="aravind@i2isoftwares.com";
                if (toMail != string.Empty)
                {
                    mailMessage.To.Add(toMail);
                    if (ccmail.Trim() != string.Empty)
                    {
                        foreach (var address in ccmail.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            if (address != string.Empty)
                            {
                                mailMessage.CC.Add(address);
                            }
                        }
                    }

                    mailMessage.From = new MailAddress(frommail, "Facility Audit - Crtical Observations");
                    mailMessage.Subject = subject;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Body = body;
                    SmtpClient smtpClient = new SmtpClient(smtpServer, portno);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
                    smtpClient.EnableSsl = true;
                    smtpClient.Send(mailMessage);

                    objbasebo.UpdateLog("Mail Sent To :" + toMail + " CC : " + ccmail);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool CheckNotApplicable(string crticalobservation)
        {
            try
            {
                bool bObservation = false;
                string[] stringArray = { "nil", "na", "n/a" };
                string value = crticalobservation.ToLower().Trim();
                int pos = Array.IndexOf(stringArray, value);
                if (pos > -1)
                {
                    bObservation = true;
                }
                return bObservation;
            }
            catch (Exception)
            {
                throw;
            }
        }


        private void SendingMail(string body, string subject, string attachment, int companyid, int locationid,
            string training, string hr, string scm, string compliance, string operations, DateTime Auditdate, int sbuid)
        {
            try
            {

                bool btraining = false, bhr = false, bscm = false, bcompliance = false, boperations = false;
                btraining = CheckNotApplicable(training);
                bhr = CheckNotApplicable(hr);
                bscm = CheckNotApplicable(scm);
                bcompliance = CheckNotApplicable(compliance);
                boperations = CheckNotApplicable(operations);
                InsertMailTrackingDetails(Auditdate, companyid, locationid, sbuid, subject, body, attachment); // Insert mail details
                DataSet dsEmailDet = getlocationmailid(companyid, locationid, btraining, bhr, bscm, bcompliance, boperations);
                if (dsEmailDet.Tables.Count > 0 && dsEmailDet.Tables[0].Rows.Count > 0)
                {
                    DataRow drEmail = dsEmailDet.Tables[0].Rows[0];
                    string toMail = drEmail["ToMail"].ToString();
                    string CcMail = drEmail["CCMail"].ToString();

                    string username = string.Empty;
                    string password = string.Empty;
                    string smtpServer = string.Empty;
                    int portno = 0;


                    MailMessage mailMessage = new MailMessage();
                    if (ConfigurationManager.AppSettings["UserName"] != null)
                    {
                        username = ConfigurationManager.AppSettings["UserName"];
                    }

                    if (ConfigurationManager.AppSettings["Password"] != null)
                    {
                        password = ConfigurationManager.AppSettings["Password"];
                    }

                    if (ConfigurationManager.AppSettings["SmtpServer"] != null)
                    {
                        smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                    }

                    if (ConfigurationManager.AppSettings["Port"] != null)
                    {
                        portno = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    }

                    //usermail = "venkadesan.n@i2isoftwares.com";
                    //ccmail="aravind@i2isoftwares.com";
                    if (toMail.Trim() != string.Empty)
                    {
                        string newccmails = string.Empty;
                        foreach (var address in toMail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            mailMessage.To.Add(address);
                        }
                        if (CcMail.Trim() != string.Empty)
                        {
                            string[] emailaddress = CcMail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                            foreach (var address in emailaddress)
                            {
                                if (address != string.Empty)
                                {
                                    if (IsValidEmail(address.Trim()))
                                    {
                                        newccmails = newccmails + address + ",";
                                        mailMessage.CC.Add(address.Trim());
                                    }
                                }
                            }
                        }
                        newccmails = newccmails.TrimEnd(',');
                        UpdateMailIds(Auditdate, locationid, newccmails, toMail, false, string.Empty);

                        mailMessage.From = new MailAddress(username, "Facility Audit");
                        mailMessage.Subject = subject;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.Body = body;
                        mailMessage.Attachments.Add(new Attachment(attachment));
                        SmtpClient smtpClient = new SmtpClient(smtpServer, portno);
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
                        smtpClient.EnableSsl = true;
                        smtpClient.Send(mailMessage);

                        //FileInfo prevFile = new FileInfo(attachment);
                        //if (prevFile.Exists)
                        //{
                        //    prevFile.Delete();
                        //}
                        UpdateMessage(Auditdate, locationid, "Mail Sent Sucessfully.", true);
                        objbasebo.UpdateLog("Mail Sent To :" + toMail + " CC : " + CcMail);
                    }
                    else
                    {
                        UpdateMailIds(Auditdate, locationid, CcMail, toMail, false, "No Email ID(s) Found.");
                    }
                }
            }
            catch (Exception ex)
            {
                UpdateMessage(Auditdate, locationid, ex.Message, false);
                throw;
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void GenerateExcel(DataSet dsRawdatas, string Getscored, string filename)
        {
            try
            {
                StringBuilder sDatas = new StringBuilder();

                float fEnteirAvg = 0;
                int iTotalScore = 0;
                int iTotalWeightage = 0;
                int iMaxScore = 0;
                {
                    sDatas.Append("<table width='100%' cellspacing='0' cellpadding='2' border = '1'>");
                    DataTable dtAudit = dsRawdatas.Tables[0];
                    DataTable dtCategory = dsRawdatas.Tables[1];
                    DataTable dtRawData = dsRawdatas.Tables[2];
                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sDatas.Append("<tr><td rowspan='5' style='border: none;' ><img style='height: 54px;width: 100px;' border='0' alt='Dusters' src='http://ifazility.com/facilityaudit/AppThemes/images/JLL%20New.png'></td><td rowspan='4' style='font-size: 24px;border: none;font-weight:bold;' align='center' colspan='6'>DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD</td>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client</td><td colspan='3'>" + drHeader["clientname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Site</td><td colspan='3'>" + drHeader["locsitename"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Site ID</td><td colspan='3'>" + GetSiteID(drHeader["locsitename"].ToString()) + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Client person</td><td colspan='3'>" + drHeader["clientperson"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the SBU</td><td colspan='3'>" + drHeader["sbuname"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the OM / AOM</td><td colspan='3'>" + drHeader["aom"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditor</td><td colspan='3'>" + drHeader["auditor"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Name of the Auditee</td><td colspan='3'>" + drHeader["auditee"].ToString() + "</td><td colspan='2'></td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Last audit date</td><td align='left' colspan='3'>" + drHeader["displastauditdate"].ToString() + "</td><td colspan='2'>Last Audit Score</td></tr>");
                    sDatas.Append("<tr><td colspan='2'>Current audit date</td><td align='left' colspan='3'>" + drHeader["dispauditdate"].ToString() + "</td><td>Current Audit Score</td><td>" + Getscored + "%</td></tr>");

                    for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                    {
                        DataRow draudit = dtAudit.Rows[iauditcount];
                        int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                        string auditname = draudit["auditname"].ToString();
                        sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>" + auditname + "</td></tr>");
                        sDatas.Append("<tr><th style = 'background-color: #FCB133;color:#ffffff'>Sl.No</th><th style = 'background-color: #FCB133;color:#ffffff'>Observations</th><th style = 'background-color: #FCB133;color:#ffffff;width: 5%;'>Maximum <br/> Score</th><th style = 'background-color: #FCB133;color:#ffffff;width: 5%;'>Score <br/> Obtained</th><th style = 'background-color: #FCB133;color:#ffffff;width: 30%;'>Auditor Remarks</th><th style = 'background-color: #FCB133;color:#ffffff'>Remarks by SBU </th><th style = 'background-color: #FCB133;color:#ffffff'>Image</th></tr>");
                        int iscore = 0;
                        int iweightage = 0;
                        int iSingleAvg = 0;
                        int iTotalAvg = 0;
                        int iAuditMax = 0;
                        DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                        for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                        {
                            int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                            string categoryname = drCategory[icategroryCount]["categoryname"].ToString();
                            sDatas.Append("<tr><td align='center'>" + (icategroryCount + 1) + "</td><td style='font-size: 18px;' colspan='6'>" + categoryname + "</td></tr>");
                            DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                            int iCategoryScore = 0;
                            int iCategoryTotal = 0;
                            int iCatMax = 0;

                            for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                            {
                                string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                string filelink = string.Empty;
                                if (imgFile.Trim() != string.Empty)
                                {
                                    string[] imgfiles = imgFile.Split('#');
                                    foreach (string files in imgfiles)
                                    {
                                        if (files.Trim() != string.Empty)
                                        {
                                            imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                            filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";
                                        }
                                    }
                                }

                                int tScore = 0;
                                int MaxScore = 0;

                                if (score == -1)
                                {
                                    score = 0;
                                    tScore = -1;
                                    weightage = 0;
                                    MaxScore = 0;
                                }
                                else
                                {
                                    MaxScore = 1;
                                }

                                iscore += score;
                                iweightage += weightage;

                                iCategoryScore += score;
                                iCategoryTotal += weightage;

                                iTotalScore += score;
                                iTotalWeightage += weightage;

                                iSingleAvg = score * weightage;
                                iTotalAvg += iSingleAvg;

                                iMaxScore += MaxScore;
                                iCatMax += MaxScore;
                                iAuditMax += MaxScore;

                                string _score = string.Empty;
                                string _maxscore = string.Empty;
                                if (tScore == 0)
                                {
                                    _score = " " + score;
                                    _maxscore = " " + MaxScore;
                                }
                                else
                                {
                                    _score = scorename;
                                    _maxscore = scorename;
                                }
                                sDatas.Append("<tr><td align='center'>" + (iRawDatacount + 1).ToString() + "</td><td>" + question + "</td><td style='width: 5%;' align='center'>" + _maxscore + "</td><td  style='width: 5%;'  align='center'>" + _score + "</td><td>" + remarks + "</td><td style='width: 30%;'></td><td>" + filelink + "</td></tr>");
                            }
                            string sCatNA = string.Empty;
                            string sCatMaxNA = string.Empty;
                            sCatNA = iCatMax.ToString();
                            sCatMaxNA = iCategoryScore.ToString();
                            if (iCatMax == 0)
                            {
                                sCatNA = "N/A";
                            }
                            if (iCategoryScore == 0)
                            {
                                sCatMaxNA = "N/A";
                            }
                            sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>" + categoryname + "</td><td align='center'>" + sCatNA + "</td><td align='center'>" + sCatMaxNA + "</td><td></td><td></td><td></td></tr>");
                        }
                        float iScoredAvg = 0;
                        iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                        fEnteirAvg += iScoredAvg;

                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Total Score for " + auditname + "</td><td align='center'>" + iAuditMax + "</td><td align='center'>" + iscore + "</td><td></td><td></td><td></td></tr>");
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td><td align='center'>" + iScoredAvg + "%" + "</td><td></td><td></td><td></td></tr>");
                    }
                    float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>GRAND TOTAL SCORE</td><td align='center'>" + iMaxScore + "</td><td align='center'>" + iTotalScore + "</td><td></td><td></td><td></td></tr>");

                    decimal scoreGot = Convert.ToDecimal(Getscored);
                    string setColour = string.Empty;
                    if (scoreGot >= 83)
                    {
                        setColour = "<td style='background-color: #00FF00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    else
                    {
                        setColour = "<td style='background-color: #FFBE00;' align='center'>" + Getscored + "%" + "</td>";
                    }
                    sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Percentage (%)</td><td></td>" + setColour + "<td></td><td></td><td></td></tr>");
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        sDatas.Append("<tr style='font-weight:bold;font-size: 18px;'><td colspan='2'>Follow-Up Date</td><td></td><td>" + dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString() + "</td><td></td><td></td><td></td></tr>");
                    }


                    string observations = drHeader["observation"].ToString();
                    string clientremarks = drHeader["feedback"].ToString();
                    observations = observations.Replace('[', ' ');
                    observations = observations.Replace(']', ' ');
                    try
                    {
                        Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                        //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                        //"2. Training  : " + objobservation.JS_Training + " break  " +
                        //"3. SCM  : " + objobservation.JS_SCM + " break " +
                        //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                        //"5. HR  : " + objobservation.JS_HR + "break " +
                        //"6. Others : " + objobservation.JS_Operation;
                        if (objobservation != null)
                        {
                            sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Other Observations</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Operations</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Training</td><td colspan='5'>" + objobservation.JS_Training + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>SCM</td><td colspan='5'>" + objobservation.JS_SCM + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Compliance</td><td colspan='5'>" + objobservation.JS_Complaince + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>HR</td><td colspan='5'>" + objobservation.JS_HR + "</td></tr>");
                            sDatas.Append("<tr style='font-weight:bold;font-size: 15px;'><td colspan='2'>Others</td><td colspan='5'>" + objobservation.JS_Operation + "</td></tr>");
                        }
                    }
                    catch (Exception) { }
                    sDatas.Append("<tr><td align='center' style='font-size: 22px;' colspan='7'>Client Feedback</td></tr>");
                    sDatas.Append("<tr><td  colspan='7'>" + clientremarks + "</td></tr>");

                    sDatas.Append("</table>");



                    string memString = sDatas.ToString();
                    // convert string to stream
                    byte[] buffer = Encoding.ASCII.GetBytes(memString);
                    MemoryStream ms = new MemoryStream(buffer);
                    //write to file
                    FileStream file = new FileStream(filename, FileMode.Create, FileAccess.Write);
                    ms.WriteTo(file);
                    file.Close();
                    ms.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void MakeExcel(DataSet dsRawdatas, string Getscored, string excelfilepath)
        {
            try
            {
                //string excelfilepath = Server.MapPath("DownloadExcels/") + Guid.NewGuid().ToString() + ".xlsx";
                FileInfo newFile = new FileInfo(excelfilepath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(excelfilepath);
                }

                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

                    //Add the headers

                    StringBuilder sDatas = new StringBuilder();

                    float fEnteirAvg = 0;
                    int iTotalScore = 0;
                    int iTotalWeightage = 0;
                    int iMaxScore = 0;

                    DataTable dtChartdet = new DataTable();
                    dtChartdet.Columns.Add("Categoryname", typeof(string));
                    dtChartdet.Columns.Add("Value", typeof(float));
                    dtChartdet.Columns.Add("auditId", typeof(int));

                    if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtAudit = dsRawdatas.Tables[0];
                        DataTable dtCategory = dsRawdatas.Tables[1];
                        DataTable dtRawData = dsRawdatas.Tables[2];
                        DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        // worksheet.Cells[1, 1].Value = "Image Here";
                        worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

                        var headerFont = worksheet.Cells[1, 1].Style.Font;
                        headerFont.Bold = true;
                        headerFont.Size = 18;
                        worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells["A1:G4"].Merge = true;

                        FileInfo fileinfo = new FileInfo(Server.MapPath("AppThemes/images/dts.png"));
                        var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                        picture.SetSize(100, 50);
                        picture.SetPosition(1, 0, 1, 0);

                        worksheet.Cells[5, 1].Value = "Name of the Client";

                        worksheet.Cells[5, 3].Value = drHeader["clientname"].ToString();
                        worksheet.Cells[5, 6].Value = "";



                        worksheet.Cells["A5:B5"].Merge = true;
                        worksheet.Cells["C5:E5"].Merge = true;
                        worksheet.Cells["F5:G5"].Merge = true;

                        int imaxrowcount = 6;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Site";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["locsitename"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Name = drHeader["locsitename"].ToString();

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Tower";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["towername"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Site ID";
                        worksheet.Cells[imaxrowcount, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Client person";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["clientperson"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the SBU";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["sbuname"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the OM / AOM";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["aom"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Auditor";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["auditor"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Name of the Auditee";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["auditee"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["F" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = "Last audit date";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["displastauditdate"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "Last Audit Score";
                        worksheet.Cells[imaxrowcount, 7].Value = drHeader["lastscore"].ToString() + "%";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        //worksheet.Cells["F13:G13"].Merge = true;

                        imaxrowcount++;

                        worksheet.Cells[imaxrowcount, 1].Value = "Current audit date";
                        worksheet.Cells[imaxrowcount, 3].Value = drHeader["dispauditdate"].ToString();
                        worksheet.Cells[imaxrowcount, 6].Value = "Current Audit Score";
                        worksheet.Cells[imaxrowcount, 7].Value = Getscored + "%";

                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells["C" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;

                        imaxrowcount++;


                        for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        {
                            DataRow draudit = dtAudit.Rows[iauditcount];
                            int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                            string auditname = draudit["auditname"].ToString();

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                            worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
                            worksheet.Cells[imaxrowcount, 2].Value = "Observations";
                            worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
                            worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
                            worksheet.Cells[imaxrowcount, 5].Value = "Auditor Remarks";
                            worksheet.Cells[imaxrowcount, 6].Value = "Remarks by Sbu";
                            worksheet.Cells[imaxrowcount, 7].Value = "Image";

                            var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 7];
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                            imaxrowcount++;

                            int iscore = 0;
                            int iweightage = 0;
                            int iSingleAvg = 0;
                            int iTotalAvg = 0;
                            int iAuditMax = 0;
                            DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                            for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                            {
                                int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                                string categoryname = drCategory[icategroryCount]["categoryname"].ToString();

                                worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
                                worksheet.Cells[imaxrowcount, 2].Value = categoryname;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

                                worksheet.Cells["B" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                                int iCategoryScore = 0;
                                int iCategoryTotal = 0;
                                int iCatMax = 0;

                                int iCatWisePercentage = 0;
                                int iCatWiseTotal = 0;

                                for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                                {
                                    string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                    string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                    int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                    int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                    string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                    string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                    string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
                                    string filelink = string.Empty;
                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
                                        if (ConfigurationManager.AppSettings["Imaglists"] != null)
                                        {
                                            filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
                                        }
                                        filelink = filepath + "?TransactionId=" + transactionid;
                                        //string[] imgfiles = imgFile.Split('#');
                                        //foreach (string files in imgfiles)
                                        //{
                                        //    if (files.Trim() != string.Empty)
                                        //    {
                                        //        imgFile = "http://ifazility.com/facilityaudit/auditimage/" + files;
                                        //        //filelink += "<a href='" + imgFile + "'>" + imgFile + "</a><br/>";

                                        //        filelink = "HYPERLINK(\"" + imgFile + "\",\"" + files + "\")";
                                        //    }
                                        //}
                                    }

                                    int tScore = 0;
                                    int MaxScore = 0;

                                    if (score == -1)
                                    {
                                        score = 0;
                                        tScore = -1;
                                        weightage = 0;
                                        MaxScore = 0;
                                    }
                                    else
                                    {
                                        MaxScore = 1;
                                    }

                                    iscore += score;
                                    iweightage += weightage;

                                    iCategoryScore += score;
                                    iCategoryTotal += weightage;

                                    iTotalScore += score;
                                    iTotalWeightage += weightage;

                                    iSingleAvg = score * weightage;
                                    iTotalAvg += iSingleAvg;

                                    iCatWisePercentage = score * weightage;
                                    iCatWiseTotal += iCatWisePercentage;

                                    iMaxScore += MaxScore;
                                    iCatMax += MaxScore;
                                    iAuditMax += MaxScore;

                                    string _score = string.Empty;
                                    string _maxscore = string.Empty;
                                    if (tScore == 0)
                                    {
                                        _score = " " + score;
                                        _maxscore = " " + MaxScore;
                                    }
                                    else
                                    {
                                        _score = scorename;
                                        _maxscore = scorename;
                                    }


                                    worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
                                    worksheet.Cells[imaxrowcount, 2].Value = question;

                                    if (question.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    }

                                    //worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

                                    int MaxRefId;
                                    int ScorerefID;

                                    bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = _maxscore;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = _score;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    //worksheet.Cells[imaxrowcount, 4].Value = _score;
                                    worksheet.Cells[imaxrowcount, 5].Value = remarks;

                                    if (remarks.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 5].Style.WrapText = true;
                                    }

                                    worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        var cell = worksheet.Cells[imaxrowcount, 7];
                                        cell.Hyperlink = new Uri(filelink);
                                        cell.Value = "ImageList";
                                    }

                                    // worksheet.Cells[imaxrowcount, 7].Formula = filelink;
                                    //worksheet.Cells[imaxrowcount, 7].Style.WrapText = true;
                                    imaxrowcount++;
                                }

                                float _CateWisTot = 0;
                                if (iCategoryTotal != 0)
                                {
                                    //_CateWisTot = ((float)(iCategoryScore * 100 / iCategoryTotal));
                                    _CateWisTot = ((float)(iCatWiseTotal * 100 / iCategoryTotal));
                                }
                                else
                                {
                                    _CateWisTot = 0;
                                }

                                dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


                                string sCatNA = string.Empty;
                                string sCatMaxNA = string.Empty;
                                sCatNA = iCatMax.ToString();
                                sCatMaxNA = iCategoryScore.ToString();
                                if (iCatMax == 0)
                                {
                                    sCatNA = "N/A";
                                }
                                if (iCategoryScore == 0)
                                {
                                    sCatMaxNA = "N/A";
                                }

                                worksheet.Cells[imaxrowcount, 1].Value = "Sub Total " + categoryname;

                                int _catena;
                                int _catmaxna;
                                bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = _catena;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }

                                worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                            float iScoredAvg = 0;
                            if (iTotalAvg > 0)
                            {
                                iScoredAvg = ((float)(iTotalAvg * 100 / iweightage));
                            }
                            fEnteirAvg += iScoredAvg;

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 3].Value = iAuditMax;
                            worksheet.Cells[imaxrowcount, 4].Value = iscore;

                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                            worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;
                        }
                        //float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                        worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
                        worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
                        worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;

                        decimal scoreGot = Convert.ToDecimal(Getscored);


                        worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = Getscored + "%";
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        string setColour = string.Empty;
                        if (scoreGot >= 83)
                        {
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
                        }
                        else
                        {
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
                        }
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;



                        if (dsRawdatas.Tables[3].Rows.Count > 0)
                        {
                            worksheet.Cells[imaxrowcount, 1].Value = "Follow-Up Date";
                            worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            worksheet.Cells[imaxrowcount, 4].Value = dsRawdatas.Tables[3].Rows[0]["dispFollowUpDate"].ToString();
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                            worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            imaxrowcount++;
                        }


                        string observations = drHeader["observation"].ToString();
                        string clientremarks = drHeader["feedback"].ToString();
                        observations = observations.Replace('[', ' ');
                        observations = observations.Replace(']', ' ');
                        try
                        {
                            Observations objobservation = JsonConvert.DeserializeObject<Observations>(observations);
                            //                string observationdet = "1. Operations : " + objobservation.JS_Operation + "break" +
                            //"2. Training  : " + objobservation.JS_Training + " break  " +
                            //"3. SCM  : " + objobservation.JS_SCM + " break " +
                            //"4. Compliance  : " + objobservation.JS_Complaince + "break" +
                            //"5. HR  : " + objobservation.JS_HR + "break " +
                            //"6. Others : " + objobservation.JS_Operation;
                            if (objobservation != null)
                            {

                                worksheet.Cells[imaxrowcount, 1].Value = "Other Observations";
                                worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Operations";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Operation;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Training";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Training;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "SCM";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_SCM;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Compliance";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Complaince;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "HR";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_HR;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                worksheet.Cells[imaxrowcount, 1].Value = "Others";
                                worksheet.Cells[imaxrowcount, 3].Value = objobservation.JS_Others;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                worksheet.Cells["C" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                        }
                        catch (Exception) { }

                        worksheet.Cells[imaxrowcount, 1].Value = "Client Feedback";
                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
                        worksheet.Cells["A" + imaxrowcount + ":G" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        // worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
                        // worksheet.Cells.AutoFitColumns(1);
                        //// worksheet.Cells.AutoFitColumns(2);
                        worksheet.Column(2).Width = 140;
                        worksheet.Column(5).Width = 140;
                        worksheet.Cells.AutoFitColumns(1);
                        worksheet.Cells.AutoFitColumns(2);
                        worksheet.Cells.AutoFitColumns(3);
                        // worksheet.Cells.AutoFitColumns(4);
                        //worksheet.Cells.AutoFitColumns(5);
                        // worksheet.Cells.AutoFitColumns(6);
                        // worksheet.Cells.AutoFitColumns(7);

                        int c1;

                        imaxrowcount = 7;
                        ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int startchar = 0;
                        int colspace = 0;
                        foreach (DataRow drAuditRow in dtAudit.Rows)
                        {
                            int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
                            string auditname = drAuditRow["auditname"].ToString();
                            DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
                            if (drAuditCategories.Length > 0)
                            {
                                DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
                                int startvalue = imaxrowcount;

                                char Firstcol = Alphabetic[startchar];
                                char Secondcol = Alphabetic[startchar + 1];

                                foreach (DataRow drchartdet in dtCategoryChart.Rows)
                                {
                                    string categoryname = drchartdet["categoryname"].ToString();
                                    decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

                                    worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
                                    worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
                                    imaxrowcount++;
                                }

                                var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
                                var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
                                //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
                                chart.SetSize(510, 300);
                                chart.Title.Text = auditname;
                                chart.SetPosition(7, 0, colspace, 0);
                                colspace += 9;

                                startchar += 2;
                            }
                        }


                        package.Save();

                        //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        //this.Response.AddHeader(
                        //          "content-disposition",
                        //          string.Format("attachment;  filename={0}", "ExcellData.xlsx"));
                        //this.Response.BinaryWrite(package.GetAsByteArray());
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #endregion

        #region "Get Mail Datatable"

        public DataSet GetRawDatasDateWiseReport(DateTime auditdate, int locationid, int sbuid, int sectorid)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetRawDatasDateWiseReport");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
                db.AddInParameter(cmd, "SectorId", DbType.Int32, sectorid);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch
            {
                throw;
            }
        }

        public void InsertMailTrackingDetails(DateTime auditdate, int companyid, int locationid, int sbuid, string subject, string body, string attachments)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_InsertMailingSystem");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
                db.AddInParameter(cmd, "Subject", DbType.String, subject);
                db.AddInParameter(cmd, "Body", DbType.String, body);
                db.AddInParameter(cmd, "Attachments", DbType.String, attachments);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateMailIds(DateTime auditdate, int locationid, string ccmail, string tomail, bool isSent, string message)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_UpdateMailIDs");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "ccmail", DbType.String, ccmail);
                db.AddInParameter(cmd, "ToMail", DbType.String, tomail);
                db.AddInParameter(cmd, "isSent", DbType.Boolean, isSent);
                db.AddInParameter(cmd, "Message", DbType.String, message);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateMessage(DateTime auditdate, int locationid, string message, bool isSent)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_UpdateMessageDet");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "Message", DbType.String, message);
                db.AddInParameter(cmd, "isSent", DbType.Boolean, isSent);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public DataSet getlocationmailid(int companyid, int locationid, bool training, bool hr, bool scm, bool compliance, bool operations)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetLocationwiseMailIDs");
                db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);
                db.AddInParameter(cmd, "bTraining", DbType.Boolean, training);
                db.AddInParameter(cmd, "bhr", DbType.Boolean, hr);
                db.AddInParameter(cmd, "bscm", DbType.Boolean, scm);
                db.AddInParameter(cmd, "bcompliance", DbType.Boolean, compliance);
                db.AddInParameter(cmd, "boperations", DbType.Boolean, operations);

                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch
            {
                throw;
            }
        }


        #endregion

        private void ByteArrayToImage(byte[] byteArrayIn, string Path)
        {
            //System.Drawing.Image.GetThumbnailImageAbort myCallBack = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
            System.Drawing.Image newImage;


            if (byteArrayIn != null)
            {
                using (MemoryStream stream = new MemoryStream(byteArrayIn))
                {
                    //Bitmap newBitmap = new Bitmap(varBmp);
                    //varBmp.Dispose();
                    //varBmp = null;

                    newImage = System.Drawing.Image.FromStream(stream);

                    //var fileName = Path.GetFileName(fileurl.FileName);
                    //fileurl.SaveAs(Path.Combine(@"c:\projects", fileName));

                    newImage.Save(Path + "." + System.Drawing.Imaging.ImageFormat.Png);
                    newImage.Dispose();
                    newImage = null;


                }
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string uploadimages(string filename, string bas64Image, string guid, string deviceid)
        {
            byte[] data = null;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataTable dtmsg = new DataTable();
            dtmsg.Columns.Add("Status");
            dtmsg.Columns.Add("Message");
            dtmsg.Columns.Add("guid");
            dtmsg.Columns.Add("deviceid");
            try
            {
                objbasebo.UpdateLog("AddFeedBackDetails Start");


                // bas64Image = images[j].TrimStart('"').TrimEnd('"');
                bas64Image = bas64Image.TrimStart('"').TrimEnd('"');
                data = Convert.FromBase64String(bas64Image);


                if (data.Length > 0)
                {

                    System.Drawing.Image newImage;
                    filename = filename.TrimEnd(',').Replace('&', ' ').Replace('/', ' ');

                    if (data != null)
                    {
                        using (MemoryStream stream = new MemoryStream(data))
                        {
                            //Bitmap newBitmap = new Bitmap(varBmp);
                            //varBmp.Dispose();
                            //varBmp = null;

                            newImage = System.Drawing.Image.FromStream(stream);

                            //var fileName = Path.GetFileName(fileurl.FileName);
                            //fileurl.SaveAs(Path.Combine(@"c:\projects", fileName));

                            newImage.Save(ConfigurationManager.AppSettings["Auditimages"] + filename);
                            newImage.Dispose();
                            newImage = null;
                            dtmsg.Rows.Add("1", "Saved Sucessfully", guid, deviceid);

                            return ConvertJavaSeriptSerializer(dtmsg);


                        }
                    }
                    //ByteArrayToImage(data, ConfigurationManager.AppSettings["Feedbackimage"] + filename);
                }
            }

            catch
            {
                dtmsg.Rows.Add("0", "Not Saved Sucessfully", guid, deviceid);
                return ConvertJavaSeriptSerializer(dtmsg);
            }

            dtmsg.Rows.Add("2", "Un defined erro", guid, deviceid);
            return ConvertJavaSeriptSerializer(dtmsg);
        }

    }

    public class Observations
    {
        public string JS_SCM { get; set; }
        public string JS_Others { get; set; }
        public string JS_HR { get; set; }
        public string JS_Complaince { get; set; }
        public string JS_Training { get; set; }
        public string JS_Operation { get; set; }
    }
}